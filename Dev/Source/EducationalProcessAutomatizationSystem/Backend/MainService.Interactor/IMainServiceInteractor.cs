﻿using EducationalProcessAutomatizationSystem.Backend.MainService.Contract;
using EducationalProcessAutomatizationSystem.Common.CommonModels;

namespace EducationalProcessAutomatizationSystem.Backend.MainService.Interactor
{
	public interface IMainServiceInteractor
	{
		Response<UserCredentials> Login(LoginCredentials loginCredentials, out bool passwordUpdateRequired);

		Response<UserCredentials> Validate(UserCredentials credentials);
	}
}
