﻿using System;
using EducationalProcessAutomatizationSystem.Backend.MainService.Contract;
using EducationalProcessAutomatizationSystem.Common.CommonModels;
using EducationalProcessAutomatizationSystem.Common.Utils.Http;

namespace EducationalProcessAutomatizationSystem.Backend.MainService.Interactor
{
	public class MainServiceInteractor : IMainServiceInteractor
	{
		public MainServiceInteractor(IHttpExecutor httpExecutor, Uri mainServiceUrl)
		{
			_httpExecutor = httpExecutor;
			_mainServiceUrl = mainServiceUrl;
		}

		public Response<UserCredentials> Login(LoginCredentials loginCredentials, out bool passwordUpdateRequired)
		{
			var result = _httpExecutor
				.Post<LoginCredentials, Response<UserCredentials>>(new Uri(_mainServiceUrl, Api.Login), loginCredentials);

			passwordUpdateRequired = result.Headers.ContainsKey(nameof(passwordUpdateRequired));

			return result.Result;
		}

		public Response<UserCredentials> Validate(UserCredentials credentials)
		{
			return _httpExecutor
				.WithBearerJwt(credentials?.Token)
				.Post<UserCredentials, Response<UserCredentials>>(new Uri(_mainServiceUrl, Api.Validate), credentials)
				.Result;
		}

		private readonly IHttpExecutor _httpExecutor;
		private readonly Uri _mainServiceUrl;
	}
}
