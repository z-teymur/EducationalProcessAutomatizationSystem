﻿using System;
using EducationalProcessAutomatizationSystem.Backend.MainService.Controllers;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Api;

namespace EducationalProcessAutomatizationSystem.Backend.MainService.Interactor
{
	public static class Api
	{
		public static Uri Login => ApiUriCreator.Create(nameof(AuthorizationController), nameof(AuthorizationController.Login));

		public static Uri Validate => ApiUriCreator.Create(nameof(ValidationController), nameof(ValidationController.Validate));
	}
}
