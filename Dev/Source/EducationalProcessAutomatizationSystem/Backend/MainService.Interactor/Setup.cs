﻿using System;
using EducationalProcessAutomatizationSystem.Common.Utils.Http;
using EducationalProcessAutomatizationSystem.Common.Utils.Json;
using Microsoft.Extensions.DependencyInjection;

namespace EducationalProcessAutomatizationSystem.Backend.MainService.Interactor
{
	public static class Setup
	{
		public static void AddMainServiceInteractor(this IServiceCollection services, Uri mainServiceUri)
		{
			services.AddScoped<IJsonSerializer, JsonSerializer>();
			services.AddScoped<IHttpExecutor, HttpExecutor>();
			services.AddScoped<IMainServiceInteractor, MainServiceInteractor>(
				provider => new MainServiceInteractor(provider.GetRequiredService<IHttpExecutor>(), mainServiceUri));
		}
	}
}
