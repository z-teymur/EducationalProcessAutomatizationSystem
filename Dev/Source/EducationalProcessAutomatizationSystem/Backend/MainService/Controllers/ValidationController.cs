﻿using System;
using System.Linq;
using System.Security.Claims;
using EducationalProcessAutomatizationSystem.Backend.Common.TokenControl;
using EducationalProcessAutomatizationSystem.Backend.MainService.Contract;
using EducationalProcessAutomatizationSystem.Backend.MainService.Resources;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Common.CommonModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EducationalProcessAutomatizationSystem.Backend.MainService.Controllers
{
	[ApiController]
	[DefaultRoute]
	public class ValidationController : ControllerBase
	{
		public ValidationController(
			ITokenComposer tokenComposer,
			ILogger<ValidationController> logger)
		{
			_tokenComposer = tokenComposer;
			_logger = logger;
		}

		[HttpPost]
		public Response<UserCredentials> Validate(UserCredentials credentials)
		{
			using var s = _logger.BeginScope(nameof(Validate));

			_logger.LogInformation($"Getting user information from token");
			var tokenUid = Guid.Parse(HttpContext.User.Identity.Name);
			var tokenPassword = HttpContext.User.Claims.SingleOrDefault(c => c.Type.Equals("password", StringComparison.Ordinal))?.Value;
			var tokenRoles = HttpContext.User.Claims
				.Where(c => c.Type.Equals(ClaimsIdentity.DefaultRoleClaimType, StringComparison.Ordinal))
				.Select(c => c.Value);

			_logger.LogInformation($"Comparing cookie account data with token account data");
			if (!credentials.Uid.Equals(tokenUid)
				|| tokenPassword == null
				|| !tokenPassword.Equals(credentials.Password, StringComparison.Ordinal))
			{
				_logger.LogInformation("Account data in token is not matching with provided account data");
				return new Response<UserCredentials>(AuthorizationResources.InvalidCredentialsProvided);
			}

			_logger.LogInformation($"Comparing cookie role data with token role data");
			if (!credentials.Roles.OrderBy(x => x).SequenceEqual(tokenRoles.OrderBy(x => x)))
			{
				_logger.LogInformation("Roles in token are not matching with provided roles");
				return new Response<UserCredentials>(AuthorizationResources.InvalidRolesProvided);
			}

			_logger.LogInformation("Creating new token");
			credentials.Token =
				_tokenComposer.Compose(credentials.Uid, credentials.Password, credentials.Roles);

			return new Response<UserCredentials>(credentials);
		}

		private readonly ILogger<ValidationController> _logger;
		private readonly ITokenComposer _tokenComposer;
	}
}
