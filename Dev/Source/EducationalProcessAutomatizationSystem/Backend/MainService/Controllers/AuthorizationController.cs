﻿using System;
using System.Linq;
using System.Text;
using EducationalProcessAutomatizationSystem.Backend.Common.TokenControl;
using EducationalProcessAutomatizationSystem.Backend.MainService.Contract;
using EducationalProcessAutomatizationSystem.Backend.MainService.Resources;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Common.CommonModels;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EducationalProcessAutomatizationSystem.Backend.MainService.Controllers
{
	[ApiController]
	[DefaultRoute]
	public class AuthorizationController : ControllerBase
	{
		public AuthorizationController(
			IAccountRepository accountRepository,
			IRoleRepository roleRepository,
			ITokenComposer tokenComposer,
			ILogger<AuthorizationController> logger)
		{
			_accountRepository = accountRepository;
			_roleRepository = roleRepository;
			_tokenComposer = tokenComposer;
			_logger = logger;
		}


		[HttpPost]
		[AllowAnonymous]
		[AnonymousOnly]
		public Response<UserCredentials> Login(LoginCredentials loginCredentials)
		{
			using var s = _logger.BeginScope(nameof(Login));

			_logger.LogInformation($"Getting account {loginCredentials.Login} from database");
			var account = _accountRepository.GetByLogin(loginCredentials.Login);

			_logger.LogInformation($"Checking account data");
			if (account == null || !Encoding.UTF8.GetString(account.Password).Equals(loginCredentials.Password, StringComparison.Ordinal))
			{
				_logger.LogInformation("Account is not in database or password is invalid");
				return new Response<UserCredentials>(AuthorizationResources.InvalidCredentialsProvided);
			}

			_logger.LogInformation("Getting roles from database");
			var roles = _roleRepository.GetByAccountUid(account.Uid);

			var userCredentials =	new UserCredentials
			{
				Uid = account.Uid,
				Password = loginCredentials.Password,
				Roles = roles.Select(x => x.Name).ToList()
			};

			_logger.LogInformation("Creating token");
			userCredentials.Token = _tokenComposer.Compose(userCredentials.Uid, userCredentials.Password, userCredentials.Roles);

			return new Response<UserCredentials>(userCredentials);
		}

		private readonly ILogger<AuthorizationController> _logger;
		private readonly IAccountRepository _accountRepository;
		private readonly IRoleRepository _roleRepository;
		private readonly ITokenComposer _tokenComposer;
	}
}
