﻿using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using EducationalProcessAutomatizationSystem.Common.Utils.Sql;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace EducationalProcessAutomatizationSystem.Backend.MainService
{
	public static class Bootstrapper
	{
		public static void RegisterDependencies(this IServiceCollection services)
		{
			services.AddScoped<ISqlExecutor, ISqlExecutor>(provider =>
				new SqlExecutor(DatabaseConnectionConfiguration.GetReadWriteConnection(nameof(MainService))));

			services.AddScoped<IAccountRepository, AccountRepository>();
			services.AddScoped<IRoleRepository, RoleRepository>();
		}
	}
}
