﻿using System;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Controllers;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Api;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Interactor
{
	public static class Api
	{
		public static Uri GetDisciplinesForCurrentUser => ApiUriCreator.Create(nameof(HomeController), nameof(HomeController.GetDisciplinesForCurrentUser));

		public static Uri GetProgramme => ApiUriCreator.Create(nameof(ProgrammeController), nameof(ProgrammeController.GetProgramme));

		public static Uri GetTimeScheduleForDisciplineInTextFormat => ApiUriCreator.Create(nameof(ProgrammeController), nameof(ProgrammeController.GetTimeScheduleForDisciplineInTextFormat));

		public static Uri SaveProgramme => ApiUriCreator.Create(nameof(ProgrammeController), nameof(ProgrammeController.SaveProgramme));

		public static Uri GetTimeScheduleForDiscipline => ApiUriCreator.Create(nameof(ProgrammeController), nameof(ProgrammeController.GetTimeScheduleForDiscipline));

		public static Uri GetAllCompetences => ApiUriCreator.Create(nameof(AdditionalInformationController), nameof(AdditionalInformationController.GetAllCompetences));

		public static Uri GetAllDisciplines => ApiUriCreator.Create(nameof(AdditionalInformationController), nameof(AdditionalInformationController.GetAllDisciplines));

		public static Uri GetDisciplineAdditionalInfo => ApiUriCreator.Create(nameof(AdditionalInformationController), nameof(AdditionalInformationController.GetDisciplineAdditionalInfo));
	}
}
