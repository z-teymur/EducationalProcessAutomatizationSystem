﻿using System;
using System.Collections.Generic;
using System.Globalization;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Contract;
using EducationalProcessAutomatizationSystem.Common.CommonModels;
using EducationalProcessAutomatizationSystem.Common.Utils.Http;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Interactor
{
	public class ProgrammesManagementSystemServiceInteractor : IProgrammesManagementSystemServiceInteractor
	{
		private readonly IHttpExecutor _httpExecutor;
		private readonly Uri _programmesManagementSystemServiceUrl;
		private string _token;

		public ProgrammesManagementSystemServiceInteractor(IHttpExecutor httpExecutor, Uri programmesManagementSystemServiceUrl)
		{
			_httpExecutor = httpExecutor;
			_programmesManagementSystemServiceUrl = programmesManagementSystemServiceUrl;
		}

		public Response<List<DisciplineListItem>> GetDisciplinesForCurrentUser()
		{
			return _httpExecutor
				.WithBearerJwt(_token)
				.Get<Response<List<DisciplineListItem>>>(new Uri(_programmesManagementSystemServiceUrl, Api.GetDisciplinesForCurrentUser))
				.Result;
		}

		public Response<Programme> GetProgramme(Guid uid)
		{
			return _httpExecutor
				.WithBearerJwt(_token)
				.Get<Response<Programme>>(
					new Uri(_programmesManagementSystemServiceUrl, Api.GetProgramme),
					(nameof(uid), uid.ToString()))
				.Result;
		}

		public Response<IEnumerable<Competence>> GetCompetences(int year)
		{
			return _httpExecutor
				.WithBearerJwt(_token)
				.Get<Response<IEnumerable<Competence>>>(
					new Uri(_programmesManagementSystemServiceUrl, Api.GetAllCompetences),
					(nameof(year), year.ToString(CultureInfo.InvariantCulture)))
				.Result;
		}

		public Response<IEnumerable<string>> GetDisciplines()
		{
			return _httpExecutor
				.WithBearerJwt(_token)
				.Get<Response<IEnumerable<string>>>(
					new Uri(_programmesManagementSystemServiceUrl, Api.GetAllDisciplines))
				.Result;
		}

		public Response<DisciplineAdditionalInfoItem> GetDisciplineAdditionalInfo(Guid disciplineUid)
		{
			return _httpExecutor
				.WithBearerJwt(_token)
				.Get<Response<DisciplineAdditionalInfoItem>>(
					new Uri(_programmesManagementSystemServiceUrl, Api.GetDisciplineAdditionalInfo),
					(nameof(disciplineUid), disciplineUid.ToString()))
				.Result;
		}

		public Response<string> GetTimeScheduleForDisciplineInTextFormat(Guid disciplineUid)
		{
			return _httpExecutor
				.WithBearerJwt(_token)
				.Get<Response<string>>(
					new Uri(_programmesManagementSystemServiceUrl, Api.GetTimeScheduleForDisciplineInTextFormat),
					(nameof(disciplineUid), disciplineUid.ToString()))
				.Result;
		}

		public Response<List<TimeSchedule>> GetTimeScheduleForDiscipline(Guid disciplineUid)
		{
			return _httpExecutor
				.WithBearerJwt(_token)
				.Get<Response<List<TimeSchedule>>>(
					new Uri(_programmesManagementSystemServiceUrl, Api.GetTimeScheduleForDiscipline),
					(nameof(disciplineUid), disciplineUid.ToString()))
				.Result;
		}

		public Response<Programme> SaveProgramme(ProgrammeWithDisciplineUid programmeWithDisciplineUid)
		{
			return _httpExecutor
				.WithBearerJwt(_token)
				.Post<ProgrammeWithDisciplineUid, Response<Programme>>(
					new Uri(_programmesManagementSystemServiceUrl, Api.SaveProgramme), programmeWithDisciplineUid)
				.Result;
		}

		public void UseBearerJwt(string token)
		{
			_token = token;
		}
	}
}