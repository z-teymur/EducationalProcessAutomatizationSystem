﻿using System;
using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Contract;
using EducationalProcessAutomatizationSystem.Common.CommonModels;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Interactor
{
	public interface IProgrammesManagementSystemServiceInteractor : IServiceInteractor
	{
		Response<List<DisciplineListItem>> GetDisciplinesForCurrentUser();

		Response<Programme> GetProgramme(Guid uid);

		Response<IEnumerable<Competence>> GetCompetences(int year);

		Response<IEnumerable<string>> GetDisciplines();

		Response<DisciplineAdditionalInfoItem> GetDisciplineAdditionalInfo(Guid disciplineUid);

		Response<string> GetTimeScheduleForDisciplineInTextFormat(Guid disciplineUid);

		Response<List<TimeSchedule>> GetTimeScheduleForDiscipline(Guid disciplineUid);

		Response<Programme> SaveProgramme(ProgrammeWithDisciplineUid programmeWithDisciplineUid);
	}
}
