﻿using System;
using EducationalProcessAutomatizationSystem.Common.Utils.Http;
using EducationalProcessAutomatizationSystem.Common.Utils.Json;
using Microsoft.Extensions.DependencyInjection;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Interactor
{
	public static class Setup
	{
		public static void AddProgrammesManagementSystemServiceInteractor(this IServiceCollection services, Uri mainServiceUri)
		{
			services.AddScoped<IJsonSerializer, JsonSerializer>();
			services.AddScoped<IHttpExecutor, HttpExecutor>();
			services.AddScoped<IProgrammesManagementSystemServiceInteractor, ProgrammesManagementSystemServiceInteractor>(
				provider => new ProgrammesManagementSystemServiceInteractor(provider.GetRequiredService<IHttpExecutor>(), mainServiceUri));
		}
	}
}
