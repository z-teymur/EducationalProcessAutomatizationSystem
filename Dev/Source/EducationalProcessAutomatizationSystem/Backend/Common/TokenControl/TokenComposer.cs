﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using EducationalProcessAutomatizationSystem.Common.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;

namespace EducationalProcessAutomatizationSystem.Backend.Common.TokenControl
{
	public class TokenComposer : ITokenComposer
	{
		public TokenComposer(ITokenKeyProvider tokenKeyProvider, IHashProvider hashProvider)
		{
			_tokenKeyProvider = tokenKeyProvider;
			_hashProvider = hashProvider;
		}

		public string Compose(Guid uid, string password, IEnumerable<string> roles)
		{
			var claims = new List<Claim>
			{
				new Claim(ClaimsIdentity.DefaultNameClaimType, uid.ToString()),
				new Claim(nameof(password), password),
			};

			foreach (var role in roles)
			{
				claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));
			}

			var key = _tokenKeyProvider.GetKey();
			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Audience = AuthorizationTokenConfiguration.Audience,
				Issuer = AuthorizationTokenConfiguration.Issuer,
				EncryptingCredentials = new EncryptingCredentials(
					new SymmetricSecurityKey(_hashProvider.GetSha256(key)),
					SecurityAlgorithms.Aes256KW,
					SecurityAlgorithms.Aes256CbcHmacSha512),
				Expires = DateTime.UtcNow.AddMonths(2),
				NotBefore = DateTime.UtcNow,
				SigningCredentials = new SigningCredentials(
					new SymmetricSecurityKey(key),
					SecurityAlgorithms.HmacSha512),
				Subject = new ClaimsIdentity(claims)
			};

			var tokenHandler = new JwtSecurityTokenHandler();
			var token = tokenHandler.CreateJwtSecurityToken(tokenDescriptor);
			return tokenHandler.WriteToken(token);
		}

		private readonly ITokenKeyProvider _tokenKeyProvider;
		private readonly IHashProvider _hashProvider;
	}
}
