﻿using System;
using System.Collections.Generic;

namespace EducationalProcessAutomatizationSystem.Backend.Common.TokenControl
{
	public interface ITokenComposer
	{
		string Compose(Guid uid, string password, IEnumerable<string> roles);
	}
}