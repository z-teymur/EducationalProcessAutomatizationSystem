﻿using System.IO;
using System.Security.Cryptography;

namespace EducationalProcessAutomatizationSystem.Backend.Common.TokenControl
{
	internal class TokenKeyProvider : ITokenKeyProvider
	{
		public TokenKeyProvider(string dataProtectionKeysLocation)
		{
			_dataProtectionKeysLocation = dataProtectionKeysLocation;
		}

		public byte[] GetKey()
		{
			var filePath = Path.Combine(_dataProtectionKeysLocation, "key");
			if (File.Exists(filePath))
			{
				return File.ReadAllBytes(filePath);
			}
			else
			{
				var buffer = new byte[512];
				using var randomNumberGenerator = RandomNumberGenerator.Create();
				randomNumberGenerator.GetBytes(buffer);
				Directory.CreateDirectory(_dataProtectionKeysLocation);
				File.WriteAllBytes(filePath, buffer);
				return buffer;
			}
		}

		private readonly string _dataProtectionKeysLocation;
	}
}
