﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace EducationalProcessAutomatizationSystem.Backend.Common.TokenControl
{
	internal class TokenEvents : JwtBearerEvents
	{
		public TokenEvents(
			ITokenValidator tokenValidator)
		{
			_tokenValidator = tokenValidator;
		}


		public override Task AuthenticationFailed(AuthenticationFailedContext context)
		{
			context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
			return Task.CompletedTask;
		}

		public override Task Forbidden(ForbiddenContext context)
		{
			context.Response.StatusCode = (int)HttpStatusCode.NotFound;
			return Task.CompletedTask;
		}

		public override Task TokenValidated(TokenValidatedContext context)
		{
			if (_tokenValidator.Validate(context))
			{
				context.Success();
			}
			else
			{
				context.Fail(string.Empty);
				context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
			}

			return Task.CompletedTask;
		}

		private readonly ITokenValidator _tokenValidator;
	}
}
