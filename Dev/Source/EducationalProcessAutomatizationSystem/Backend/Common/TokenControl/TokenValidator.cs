﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace EducationalProcessAutomatizationSystem.Backend.Common.TokenControl
{
	internal class TokenValidator : ITokenValidator
	{
		public TokenValidator(
			IAccountRepository accountRepository,
			IRoleRepository roleRepository)
		{
			_accountRepository = accountRepository;
			_roleRepository = roleRepository;
		}


		public bool Validate(TokenValidatedContext context)
		{
			var uid = Guid.Parse(context.Principal.Identity.Name);
			var password = context.Principal.Claims.SingleOrDefault(c => c.Type.Equals("password", StringComparison.Ordinal))?.Value;
			var userRoles = context.Principal.Claims
				.Where(c => c.Type.Equals(ClaimsIdentity.DefaultRoleClaimType, StringComparison.Ordinal))
				.Select(c => c.Value)
				.OrderBy(x => x);
			var account = _accountRepository.GetByUid(uid);

			if (account != null && Encoding.UTF8.GetString(account.Password).Equals(password, StringComparison.Ordinal))
			{
				var roles = _roleRepository.GetByAccountUid(account.Uid)
					.Select(r => r.Name)
					.OrderBy(x => x).ToList();
				if (userRoles.SequenceEqual(roles))
				{
					return true;
				}
			}

			return false;
		}

		private readonly IRoleRepository _roleRepository;
		private readonly IAccountRepository _accountRepository;
	}
}
