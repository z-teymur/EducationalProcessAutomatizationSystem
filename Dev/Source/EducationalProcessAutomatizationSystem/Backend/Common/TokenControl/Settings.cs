﻿using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using EducationalProcessAutomatizationSystem.Common.Security.Cryptography;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace EducationalProcessAutomatizationSystem.Backend.Common.TokenControl
{
	public static class Settings
	{
		public static void AddTokenAuthorizationSettings(this IServiceCollection services)
		{
			services.AddScoped<IAccountRepository, AccountRepository>();
			services.AddScoped<IRoleRepository, RoleRepository>();
			services.AddScoped<ITokenKeyProvider, TokenKeyProvider>(provider =>
				new TokenKeyProvider(AuthorizationTokenConfiguration.DataProtectionKeysLocation));
			services.AddScoped<IHashProvider, HashProvider>();
			services.AddScoped<ITokenComposer, TokenComposer>();
			services.AddScoped<ITokenValidator, TokenValidator>();
			services.AddScoped<TokenEvents>();

			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
				.AddJwtBearer(options =>
				{
					var provider = services.BuildServiceProvider();
					var key = provider.GetService<ITokenKeyProvider>().GetKey();
					var hashedKey = provider.GetService<IHashProvider>().GetSha256(key);

					options.EventsType = typeof(TokenEvents);
					options.RequireHttpsMetadata = false;
					options.TokenValidationParameters = new TokenValidationParameters
					{
						ValidateIssuer = true,
						ValidIssuer = AuthorizationTokenConfiguration.Issuer,

						ValidateAudience = true,
						ValidAudience = AuthorizationTokenConfiguration.Audience,

						ValidateLifetime = true,

						IssuerSigningKey = new SymmetricSecurityKey(key),
						ValidateIssuerSigningKey = true,

						TokenDecryptionKey = new SymmetricSecurityKey(hashedKey)
					};
				});

			services.AddMvc(setup =>
			{
				var policy = new AuthorizationPolicyBuilder()
					.RequireAuthenticatedUser()
					.Build();
				setup.Filters.Add(new AuthorizeFilter(policy));
			});
		}
	}
}
