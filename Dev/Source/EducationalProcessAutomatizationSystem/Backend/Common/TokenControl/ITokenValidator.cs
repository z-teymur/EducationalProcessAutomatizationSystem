﻿using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace EducationalProcessAutomatizationSystem.Backend.Common.TokenControl
{
	internal interface ITokenValidator
	{
		bool Validate(TokenValidatedContext context);
	}
}