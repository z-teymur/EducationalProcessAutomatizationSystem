﻿namespace EducationalProcessAutomatizationSystem.Backend.Common.TokenControl
{
	public interface ITokenKeyProvider
	{
		byte[] GetKey();
	}
}