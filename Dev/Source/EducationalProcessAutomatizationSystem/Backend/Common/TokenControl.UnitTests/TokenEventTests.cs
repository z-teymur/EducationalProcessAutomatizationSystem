﻿using System.Net;
using AutoFixture;
using EducationalProcessAutomatizationSystem.Backend.Common.TokenControl;
using FluentAssertions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TokenControl.UnitTests
{
	[TestClass]
	public class TokenEventTests
	{
		private Mock<ITokenValidator> _tokenValidator;
		private Fixture _fixture;
		private Mock<HttpContext> _httpContext;
		private TokenEvents _target;

		[TestInitialize]
		public void TestInitialize()
		{
			_tokenValidator = new Mock<ITokenValidator>(MockBehavior.Strict);
			_fixture = new Fixture();

			_fixture.Register(() => new AuthenticationScheme(
				JwtBearerDefaults.AuthenticationScheme,
				null,
				new Mock<IAuthenticationHandler>().Object.GetType()));

			_fixture.Register(() => new JwtBearerOptions());

			_httpContext = new Mock<HttpContext>(MockBehavior.Strict);

			_fixture.Register(() => _httpContext.Object);




			_target = new TokenEvents(_tokenValidator.Object);
		}

		[TestMethod]
		public void AuthenticationFailedTest()
		{
			var context = _fixture.Create<AuthenticationFailedContext>();

			_httpContext.SetupSet(c => c.Response.StatusCode = (int)HttpStatusCode.Unauthorized);

			var result = _target.AuthenticationFailed(context);

			result.IsCompleted.Should().BeTrue();

			_httpContext.VerifySet(c => c.Response.StatusCode = (int)HttpStatusCode.Unauthorized, Times.Once);
		}

		[TestMethod]
		public void ForbiddenTest()
		{
			var context = _fixture.Create<ForbiddenContext>();

			_httpContext.SetupSet(c => c.Response.StatusCode = (int)HttpStatusCode.NotFound);

			var result = _target.Forbidden(context);

			result.IsCompleted.Should().BeTrue();

			_httpContext.VerifySet(c => c.Response.StatusCode = (int)HttpStatusCode.NotFound, Times.Once);
		}

		[TestMethod]
		public void TokenValidatedSuccessTest()
		{
			var context = _fixture
				.Build<TokenValidatedContext>()
				.With(tvc => tvc.SecurityToken, new Mock<SecurityToken>().Object)
				.Create();

			_tokenValidator.Setup(tv => tv.Validate(context)).Returns(true);

			var result = _target.TokenValidated(context);

			result.IsCompleted.Should().BeTrue();

			_tokenValidator.Verify(tv => tv.Validate(context), Times.Once);
		}

		[TestMethod]
		public void TokenValidatedFailTest()
		{
			var context = _fixture
				.Build<TokenValidatedContext>()
				.With(tvc => tvc.SecurityToken, new Mock<SecurityToken>().Object)
				.Create();

			_tokenValidator.Setup(tv => tv.Validate(context)).Returns(false);
			_httpContext.SetupSet(c => c.Response.StatusCode = (int)HttpStatusCode.Unauthorized);

			var result = _target.TokenValidated(context);

			result.IsCompleted.Should().BeTrue();

			_tokenValidator.Verify(tv => tv.Validate(context), Times.Once);
			_httpContext.VerifySet(c => c.Response.StatusCode = (int)HttpStatusCode.Unauthorized, Times.Once);
		}
	}
}
