﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using AutoFixture;
using EducationalProcessAutomatizationSystem.Backend.Common.TokenControl;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories;
using FluentAssertions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TokenControl.UnitTests
{
	[TestClass]
	public class TokenValidatorTests
	{
		private Mock<IAccountRepository> _accountRepository;
		private Mock<IRoleRepository> _roleRepository;
		private Fixture _fixture;
		private TokenValidator _target;
		private Mock<HttpContext> _httpContext;

		[TestInitialize]
		public void TestInitialize()
		{
			_accountRepository = new Mock<IAccountRepository>(MockBehavior.Strict);
			_roleRepository = new Mock<IRoleRepository>(MockBehavior.Strict);
			_fixture = new Fixture();

			_fixture.Register(() => new AuthenticationScheme(
				JwtBearerDefaults.AuthenticationScheme,
				null,
				new Mock<IAuthenticationHandler>().Object.GetType()));

			_fixture.Register(() => new JwtBearerOptions());

			_httpContext = new Mock<HttpContext>(MockBehavior.Strict);

			_fixture.Register(() => _httpContext.Object);


			_target = new TokenValidator(_accountRepository.Object, _roleRepository.Object);
		}

		[TestMethod]
		public void ValidateSuccsecTest()
		{
			var context = _fixture.Build<TokenValidatedContext>()
				.Without(tvc => tvc.SecurityToken)
				.Create();
			var uid = Guid.NewGuid();
			var password = _fixture.Create<string>();
			var roles = _fixture.CreateMany<string>().ToList();

			var claims = new List<Claim>
			{
				new Claim(ClaimsIdentity.DefaultNameClaimType, uid.ToString()),
				new Claim("password", password)
			};
			claims.AddRange(roles.Select(r => new Claim(ClaimsIdentity.DefaultRoleClaimType, r)));
			context.Principal = new ClaimsPrincipal(new ClaimsIdentity(claims));

			_accountRepository.Setup(ar => ar.GetByUid(uid))
				.Returns(new Account
				{
					Password = Encoding.UTF8.GetBytes(password),
					Uid = uid
				});
			_roleRepository.Setup(rr => rr.GetByAccountUid(uid))
				.Returns(roles.Select(r => new Role { Name = r }));

			var result = _target.Validate(context);

			result.Should().BeTrue();

			_accountRepository.Verify(ar => ar.GetByUid(uid), Times.Once);
			_roleRepository.Verify(rr => rr.GetByAccountUid(uid), Times.Once);
		}

		[TestMethod]
		public void ValidateNoRolesTest()
		{
			var context = _fixture.Build<TokenValidatedContext>()
				.Without(tvc => tvc.SecurityToken)
				.Create();
			var uid = Guid.NewGuid();
			var password = _fixture.Create<string>();
			var roles = _fixture.CreateMany<string>().ToList();

			var claims = new List<Claim>
			{
				new Claim(ClaimsIdentity.DefaultNameClaimType, uid.ToString()),
				new Claim("password", password)
			};
			claims.AddRange(roles.Select(r => new Claim(ClaimsIdentity.DefaultRoleClaimType, r)));
			context.Principal = new ClaimsPrincipal(new ClaimsIdentity(claims));

			_accountRepository.Setup(ar => ar.GetByUid(uid))
				.Returns(new Account
				{
					Password = Encoding.UTF8.GetBytes(password),
					Uid = uid
				});
			_roleRepository.Setup(rr => rr.GetByAccountUid(uid))
				.Returns(Array.Empty<Role>());

			var result = _target.Validate(context);

			result.Should().BeFalse();

			_accountRepository.Verify(ar => ar.GetByUid(uid), Times.Once);
			_roleRepository.Verify(rr => rr.GetByAccountUid(uid), Times.Once);
		}
	}
}
