﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using AutoFixture;
using EducationalProcessAutomatizationSystem.Backend.Common.TokenControl;
using EducationalProcessAutomatizationSystem.Common.Security.Cryptography;
using FluentAssertions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TokenControl.UnitTests
{
	[TestClass]
	public class TokenComposerTests
	{
		[TestInitialize]
		public void TestInitialize()
		{
			_tokenKeyProvider = new Mock<ITokenKeyProvider>(MockBehavior.Strict);
			_hashProvider = new Mock<IHashProvider>(MockBehavior.Strict);
			_target = new TokenComposer(_tokenKeyProvider.Object, _hashProvider.Object);
			_fixture = new Fixture();
		}

		[TestMethod]
		public void ComposeTest()
		{
			var uid = _fixture.Create<Guid>();
			var password = _fixture.Create<string>();
			var roles = _fixture.CreateMany<string>().ToList();

			var keyBytes = _fixture.CreateMany<byte>(512).ToArray();
			var hashBytes = _fixture.CreateMany<byte>(32).ToArray();

			_tokenKeyProvider.Setup(tkp => tkp.GetKey()).Returns(keyBytes);
			_hashProvider.Setup(hp => hp.GetSha256(keyBytes)).Returns(hashBytes);

			var result = _target.Compose(uid, password, roles);

			_tokenKeyProvider.Verify(tkp => tkp.GetKey(), Times.Once);
			_hashProvider.Verify(hp => hp.GetSha256(keyBytes), Times.Once);

			var claims = new JwtSecurityTokenHandler()
				.ValidateToken(
					result,
					new TokenValidationParameters
					{
						ValidateIssuer = false,
						ValidateAudience = false,
						ValidateLifetime = false,
						TokenDecryptionKey = new SymmetricSecurityKey(hashBytes),
						IssuerSigningKey = new SymmetricSecurityKey(keyBytes),
						ValidateIssuerSigningKey = true,
					},
					out SecurityToken validatedToken).Claims
				.Select(x => new Claim(x.Type, x.Value))
				.ToList();

			claims.Should().NotBeNull();
			claims.Should().ContainEquivalentOf(new Claim(ClaimsIdentity.DefaultNameClaimType, uid.ToString()));
			claims.Should().ContainEquivalentOf(new Claim(nameof(password), password));
			roles.ForEach(role => claims.Should().ContainEquivalentOf(new Claim(ClaimsIdentity.DefaultRoleClaimType, role)));
		}

		private Mock<ITokenKeyProvider> _tokenKeyProvider;
		private TokenComposer _target;
		private Fixture _fixture;
		private Mock<IHashProvider> _hashProvider;
	}
}
