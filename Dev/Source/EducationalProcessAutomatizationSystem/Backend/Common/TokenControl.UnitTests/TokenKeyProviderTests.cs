﻿using System.IO;
using System.Linq;
using AutoFixture;
using EducationalProcessAutomatizationSystem.Backend.Common.TokenControl;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TokenControl.UnitTests
{
	[TestClass]
	public class TokenKeyProviderTests
	{
		private Fixture _fixture;
		private string _directory;
		private TokenKeyProvider _target;

		[TestInitialize]
		public void TestInitialize()
		{
			_fixture = new Fixture();
			_directory = _fixture.Create<string>();
			_target = new TokenKeyProvider(_directory);
		}

		[TestMethod]
		public void GetKeyFileExistsTest()
		{
			var bytes = _fixture.CreateMany<byte>(512).ToArray();

			Directory.CreateDirectory(_directory);
			File.WriteAllBytes(Path.Combine(_directory, "key"), bytes);

			var result = _target.GetKey();

			result.Should().BeEquivalentTo(bytes);
		}

		[TestMethod]
		public void GetKeyFileNotExistsTest()
		{
			var result = _target.GetKey();
			var bytes = File.ReadAllBytes(Path.Combine(_directory, "key"));

			result.Length.Should().Be(512);
			result.Should().BeEquivalentTo(bytes);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			Directory.Delete(_directory, true);
		}
	}
}
