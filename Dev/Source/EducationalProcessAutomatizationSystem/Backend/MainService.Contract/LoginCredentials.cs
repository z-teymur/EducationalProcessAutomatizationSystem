﻿namespace EducationalProcessAutomatizationSystem.Backend.MainService.Contract
{
	public class LoginCredentials
	{
		public string Login { get; set; }

		public string Password { get; set; }
	}
}
