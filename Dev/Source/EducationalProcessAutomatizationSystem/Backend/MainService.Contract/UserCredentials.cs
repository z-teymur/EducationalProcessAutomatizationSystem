﻿using System;
using System.Collections.Generic;

namespace EducationalProcessAutomatizationSystem.Backend.MainService.Contract
{
	public class UserCredentials
	{
		public Guid Uid { get; set; }

		public string Password { get; set; }

		public List<string> Roles { get; set; }

		public string Token { get; set; }
	}
}
