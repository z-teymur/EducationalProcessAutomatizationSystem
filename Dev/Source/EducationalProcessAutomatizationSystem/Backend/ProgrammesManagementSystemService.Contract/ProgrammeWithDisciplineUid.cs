﻿using System;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Contract
{
	public class ProgrammeWithDisciplineUid
	{
		public Programme Programme { get; set; }

		public Guid DisciplineUid { get; set; }
	}
}
