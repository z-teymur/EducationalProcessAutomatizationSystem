﻿using System;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Contract
{
	public class DisciplineListItem
	{
		public string SpecialityCode { get; set; }

		public string SpecialityName { get; set; }

		public string ProfileName { get; set; }

		public string DisciplineName { get; set; }

		public Guid DisciplineUid { get; set; }

		public Guid? ProgrammeUid { get; set; }
	}
}
