﻿namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Contract
{
	public class Competence
	{
		public string Code { get; set; }

		public string Description { get; set; }
	}
}
