﻿using System;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Contract
{
	public class Programme
	{
		public Guid Uid { get; set; }

		public string Content { get; set; } = default!;

		public string RowVersion { get; set; } = default!;
	}
}
