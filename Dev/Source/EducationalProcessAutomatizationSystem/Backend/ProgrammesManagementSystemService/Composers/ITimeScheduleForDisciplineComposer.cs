﻿using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Composers
{
	public interface ITimeScheduleForDisciplineComposer
	{
		string Compose(List<TimeSchedule> semesters);
	}
}