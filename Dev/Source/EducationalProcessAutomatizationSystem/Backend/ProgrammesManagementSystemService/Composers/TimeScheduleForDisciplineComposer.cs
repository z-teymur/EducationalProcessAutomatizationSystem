﻿using System;
using System.Collections.Generic;
using System.Linq;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Composers
{
	public class TimeScheduleForDisciplineComposer : ITimeScheduleForDisciplineComposer
	{
		public string Compose(List<TimeSchedule> semesters)
		{
			var result = string.Empty;

			var generalHours = semesters.Sum(sem => sem.Lectures)
							   + semesters.Sum(sem => sem.Seminars)
							   + semesters.Sum(sem => sem.Practices)
							   + semesters.Sum(sem => sem.Laboratorys)
							   + semesters.Sum(sem => sem.Individuals)
							   + semesters.Sum(sem => sem.Controlls)
							   + semesters.Sum(sem => sem.Interactives);
			var generalSelfStudyHours = semesters.Sum(sem => sem.Individuals);
			var generalZET = Math.Round((double)generalHours / 36, 2);
			result = $"Общая трудоемкость дисциплины составляет {generalZET} {GetStringZET(generalZET)}, т.е. " +
					 $"{generalHours} {GetStringAcademicHours(generalHours)} " +
					 $"(из них {generalSelfStudyHours} {GetStringHours(generalSelfStudyHours)} – самостоятельная работа студентов).\n";

			semesters.ForEach(sem =>
			{
				int curs = Convert.ToInt32(Math.Round(((double)sem.Term / 2) + 0.1));
				int generalHours = sem.Lectures + sem.Seminars + sem.Practices + sem.Laboratorys + sem.Individuals +
								   sem.Controlls + sem.Interactives;
				double zet = Math.Round((double)generalHours / 36, 2);

				result +=
					$"На {GetStringCursNumber(curs)} курсе {GetStringTermNumberDuringCurs(sem.Term)} семестре " +
					$"выделяется {zet} {GetStringZET(zet)}, т.е. {generalHours} {GetStringAcademicHours(generalHours)} " +
					$"(из них {sem.Individuals} {GetStringHours(sem.Individuals)} – самостоятельная работа студентов).\n";
			});

			semesters.ForEach(sem =>
			{
				string controlForm = string.Empty;
				int lecturesHoursInWeek = sem.Lectures / 18;
				int labsHoursInWeek = sem.Laboratorys / 18;
				if (sem.Exams > 0)
				{
					controlForm = "Экзамен";
				}
				else if (sem.Term > 0)
				{
					controlForm = "Зачет";
				}
				else
				{
					throw new NotSupportedException();
				}

				result += $"{GetStringSemestrNumber(sem.Term)} семестр: " +
						  $"лекции – {lecturesHoursInWeek} {GetStringHours(lecturesHoursInWeek)} в неделю " +
						  $"({sem.Lectures} {GetStringHours(sem.Lectures)}), " +
						  $"лабораторные работы – {labsHoursInWeek} {GetStringHours(labsHoursInWeek)} в неделю " +
						  $"({sem.Laboratorys} {GetStringHours(sem.Laboratorys)}), " +
						  $"форма контроля – {controlForm}.";
				if (semesters.IndexOf(sem) != semesters.Count - 1)
				{
					result += "\n";
				}
			});
			return result;
		}

		#region Подборка окончаний

		private static string GetStringZET(double zet)
		{
			string[] variants = { "зачетных единиц", "зачетную единицу", "зачетные единицы" };
			return variants[(int)GrammerVariator.GetSuffixTypeOfNumber(zet)];
		}

		private static string GetStringAcademicHours(int hours)
		{
			string[] variants = { "академических часов", "академический час", "академических часа" };
			return variants[(int)GrammerVariator.GetSuffixTypeOfNumber(hours)];
		}

		private static string GetStringHours(int hours)
		{
			string[] variants = { "часов", "час", "часа" };
			return variants[(int)GrammerVariator.GetSuffixTypeOfNumber(hours)];
		}

		private static string GetStringCursNumber(int curs)
		{
			string[] variants = { string.Empty, "первом", "втором", "третьем", "четвертом", "пятом" };
			return variants[curs];
		}

		private static string GetStringTermNumberDuringCurs(int number)
		{
			string[] variants =
			{
				string.Empty, "в первом", "во втором", "в третьем", "в четвертом", "в пятом", "в шестом", "в седьмом",
				"в восьмом", "в девятом", "в десятом"
			};
			return variants[number];
		}

		private static string GetStringSemestrNumber(int number)
		{
			string[] variants =
			{
				string.Empty, "Первый", "Второй", "Третий", "Четвертый", "Пятый", "Шестой", "Седьмой", "Восьмой",
				"Девятый", "Десятый"
			};
			return variants[number];
		}

		#endregion

		private enum SuffixType
		{
			LikeZeroInTheEnd,
			LikeOneInTheEnd,
			Another
		}

		private static class GrammerVariator
		{
			public static SuffixType GetSuffixTypeOfNumber(double number)
			{
				if (number % 1 != 0 || (number % 100 > 4 && number % 100 < 21) || number % 10 > 4 || number % 10 == 0)
				{
					return SuffixType.LikeZeroInTheEnd;
				}
				else if (number % 10 == 1)
				{
					return SuffixType.LikeOneInTheEnd;
				}
				else
				{
					return SuffixType.Another;
				}
			}
		}
	}
}