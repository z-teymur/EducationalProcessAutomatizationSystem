﻿using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Composers;
using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using EducationalProcessAutomatizationSystem.Common.Utils.Sql;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService
{
	public static class Bootstrapper
	{
		public static void RegisterDependencies(this IServiceCollection services)
		{
			services.AddScoped<ISqlExecutor, ISqlExecutor>(provider =>
				new SqlExecutor(DatabaseConnectionConfiguration.GetReadWriteConnection(nameof(ProgrammesManagementSystemService))));

			services.AddScoped<IDisciplineRepository, DisciplineRepository>();
			services.AddScoped<IProgrammeRepository, ProgrammeRepository>();
			services.AddScoped<ICompetenceRepository, CompetenceRepository>();
			services.AddScoped<ITimeScheduleRepository, TimeScheduleRepository>();

			services.AddScoped<ITimeScheduleForDisciplineComposer, TimeScheduleForDisciplineComposer>();
		}
	}
}
