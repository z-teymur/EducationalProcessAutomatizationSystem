﻿using System;
using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Common.CommonModels;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Models;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Controllers
{
	[ApiController]
	[DefaultRoute]
	public class HomeController : ControllerBase
	{
		public HomeController(ILogger<HomeController> logger, IDisciplineRepository disciplineRepository)
		{
			_logger = logger;
			_disciplineRepository = disciplineRepository;
		}

		[HttpGet]
		public Response<List<DisciplineListItem>> GetDisciplinesForCurrentUser()
		{
			var disciplineListItems = _disciplineRepository.GetByAccountUid(Guid.Parse(User.Identity.Name));

			return new Response<List<DisciplineListItem>>(disciplineListItems);
		}

		private readonly ILogger<HomeController> _logger;
		private readonly IDisciplineRepository _disciplineRepository;
	}
}
