﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Composers;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Contract;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Resources;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Common.CommonModels;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Exceptions;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Programme = EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities.Programme;
using TimeSchedule = EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities.TimeSchedule;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Controllers
{
	[ApiController]
	[DefaultRoute]
	public class ProgrammeController : ControllerBase
	{
		public ProgrammeController(
			ILogger<ProgrammeController> logger,
			IProgrammeRepository programmeRepository,
			ITimeScheduleRepository timeScheduleRepository,
			IDisciplineRepository disciplineRepository,
			ITimeScheduleForDisciplineComposer timeScheduleForDisciplineComposer)
		{
			_logger = logger;
			_programmeRepository = programmeRepository;
			_timeScheduleRepository = timeScheduleRepository;
			_timeScheduleForDisciplineComposer = timeScheduleForDisciplineComposer;
			_disciplineRepository = disciplineRepository;
		}

		[HttpGet]
		public Response<Contract.Programme> GetProgramme(Guid uid)
		{
			using var s = _logger.BeginScope(nameof(GetProgramme));

			_logger.LogInformation($"Getting programme {uid} from database");
			var programme = _programmeRepository.GetByUid(uid);

			_logger.LogInformation("Returning result");
			if (programme == null)
			{
				return new Response<Contract.Programme>((Contract.Programme)null);
			}

			return new Response<Contract.Programme>(
				new Contract.Programme
				{
					Uid = programme.Uid,
					Content = Encoding.UTF8.GetString(programme.Content),
					RowVersion = Convert.ToBase64String(programme.RowVersion)
				});
		}

		[HttpGet]
		public Response<string> GetTimeScheduleForDisciplineInTextFormat(Guid disciplineUid)
		{
			using var s = _logger.BeginScope(nameof(GetTimeScheduleForDisciplineInTextFormat));

			_logger.LogInformation($"Getting time schedule for discipline {disciplineUid} from database");
			var timeSchedule = _timeScheduleRepository.GetByDisciplineUid(disciplineUid);

			_logger.LogInformation($"Transforming data to text format");
			var timeScheduleForDiscipline = _timeScheduleForDisciplineComposer.Compose(timeSchedule);

			_logger.LogInformation("Returning result");
			return new Response<string>
			{
				Data = timeScheduleForDiscipline
			};
		}

		[HttpGet]
		public Response<List<TimeSchedule>> GetTimeScheduleForDiscipline(Guid disciplineUid)
		{
			using var s = _logger.BeginScope(nameof(GetTimeScheduleForDisciplineInTextFormat));

			_logger.LogInformation($"Getting time schedule for discipline {disciplineUid} from database");
			var timeSchedule = _timeScheduleRepository.GetByDisciplineUid(disciplineUid);

			_logger.LogInformation("Returning result");
			return new Response<List<TimeSchedule>>(timeSchedule);
		}

		[HttpPost]
		public Response<Contract.Programme> SaveProgramme(ProgrammeWithDisciplineUid programmeWithDisciplineUid)
		{
			using var s = _logger.BeginScope(nameof(GetTimeScheduleForDisciplineInTextFormat));
			using var transactionScope = new TransactionScope();

			try
			{
				_logger.LogInformation($"Saving programme {programmeWithDisciplineUid.Programme.Uid}");
				var updatedProgrammeUid = _programmeRepository.Save(new Programme
				{
					Content = Encoding.UTF8.GetBytes(programmeWithDisciplineUid.Programme.Content),
					RowVersion = Convert.FromBase64String(programmeWithDisciplineUid.Programme.RowVersion ?? string.Empty),
					Uid = programmeWithDisciplineUid.Programme.Uid
				});

				if (programmeWithDisciplineUid.Programme.Uid != updatedProgrammeUid)
				{
					_logger.LogInformation($"Udpating discipline {programmeWithDisciplineUid.DisciplineUid}. Linking with programme {updatedProgrammeUid}");
					_disciplineRepository.LinkWithProgramme(programmeWithDisciplineUid.DisciplineUid, updatedProgrammeUid);
				}

				var programme = GetProgramme(updatedProgrammeUid);

				transactionScope.Complete();
				_logger.LogInformation($"Success");
				return programme;
			}
			catch (OptimisticUpdateException ex)
			{
				_logger.LogWarning(nameof(OptimisticUpdateException), ex);
				return new Response<Contract.Programme>(DBMessages.OptimisticUpdate);
			}
		}


		private readonly ILogger<ProgrammeController> _logger;
		private readonly IProgrammeRepository _programmeRepository;
		private readonly ITimeScheduleRepository _timeScheduleRepository;
		private readonly ITimeScheduleForDisciplineComposer _timeScheduleForDisciplineComposer;
		private readonly IDisciplineRepository _disciplineRepository;
	}
}
