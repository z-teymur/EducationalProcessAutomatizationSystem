﻿using System;
using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Common.CommonModels;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Models;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Controllers
{
	[ApiController]
	[DefaultRoute]
	public class AdditionalInformationController : ControllerBase
	{
		private readonly ICompetenceRepository _competenceRepository;
		private readonly ILogger<AdditionalInformationController> _logger;
		private readonly IDisciplineRepository _disciplineRepository;

		public AdditionalInformationController(
			ILogger<AdditionalInformationController> logger,
			ICompetenceRepository competenceRepository,
			IDisciplineRepository disciplineRepository)
		{
			_logger = logger;
			_competenceRepository = competenceRepository;
			_disciplineRepository = disciplineRepository;
		}

		[HttpGet]
		public Response<IEnumerable<Competence>> GetAllCompetences(int year)
		{
			_logger.LogInformation($"Getting all competences of {year} year");
			var competences = _competenceRepository.GetAll(year);

			_logger.LogInformation($"Returning result");
			return new Response<IEnumerable<Competence>>(competences);
		}

		[HttpGet]
		public Response<IEnumerable<string>> GetAllDisciplines()
		{
			_logger.LogInformation($"Getting all disciplines");
			var disciplines = _disciplineRepository.GetAll();

			_logger.LogInformation($"Returning result");
			return new Response<IEnumerable<string>>(disciplines);
		}

		[HttpGet]
		public Response<DisciplineAdditionalInfoItem> GetDisciplineAdditionalInfo(Guid disciplineUid)
		{
			_logger.LogInformation($"Getting discipline additional info {disciplineUid}");
			var disciplines = _disciplineRepository.GetAdditionalInfo(disciplineUid);

			_logger.LogInformation($"Returning result");
			return new Response<DisciplineAdditionalInfoItem>(disciplines);
		}
	}
}
