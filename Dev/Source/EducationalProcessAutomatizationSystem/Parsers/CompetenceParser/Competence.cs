﻿namespace EducationalProcessAutomatizationSystem.Parsers.CompetenceParser
{
	public class Competence
	{
		public int Code { get; private set; }

		public string Type { get; private set; }

		public string Description { get; private set; }


		public Competence(string type, int code, string description)
		{
			Code = code;
			Type = type;
			Description = description;
		}

		public override string ToString()
		{
			return $"{Type}-{Code} {Description}";
		}
	}
}