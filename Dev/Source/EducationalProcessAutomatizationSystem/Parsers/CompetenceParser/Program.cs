﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ColoredConsole;

namespace EducationalProcessAutomatizationSystem.Parsers.CompetenceParser
{
	public class Program
	{
		private static void Main()
		{
			ColorConsole.Write("Enter full path: ");
			var path = Console.ReadLine();
			path = string.IsNullOrEmpty(path) ? "Examples/Example.xlsx" : path;
			if (!File.Exists(path))
			{
				ColorConsole.WriteLine("File not found".Red());
				return;
			}

			var supportedFileFormats = new List<string> { ".xlsx", ".xls", ".xlsm" };
			if (!supportedFileFormats.Contains(Path.GetExtension(path)))
			{
				ColorConsole.WriteLine($"Supported file formats are {string.Join(' ', supportedFileFormats)}".Red());
			}

			var fileBytes = File.ReadAllBytes(path);

			ColorConsole.WriteLine("Competences: ".Green());
			new Parser()
				.ParseCompetences(fileBytes)
				.ToList()
				.ForEach(element =>
					Console.WriteLine(element.ToString()));
		}
	}
}