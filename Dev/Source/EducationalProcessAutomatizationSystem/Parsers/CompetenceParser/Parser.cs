﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using OfficeOpenXml;

namespace EducationalProcessAutomatizationSystem.Parsers.CompetenceParser
{
	public class Parser
	{
		public IEnumerable<Competence> ParseCompetences(byte[] file)
		{
			using var fileMemoryStream = new MemoryStream(file);
			using var excelPackage = new ExcelPackage(fileMemoryStream);
			using var worksheet = excelPackage.Workbook.Worksheets.First();

			var competenceCell = worksheet.Cells
				.Single(cell => cell.Text.Equals("Компетенция", StringComparison.Ordinal)).Start;
			var descriptionCell = worksheet.Cells
				.Single(cell => cell.Text.Equals("Содержание", StringComparison.Ordinal)).Start;

			var result = new List<Competence>();

			for (var row = competenceCell.Row + 1; row <= worksheet.Dimension.End.Row; row++)
			{
				var competence = worksheet.Cells[row, competenceCell.Column].Text;
				competence = competence
					.Replace("–", "-", StringComparison.InvariantCulture)
					.Replace("—", "-", StringComparison.InvariantCulture);

				var splitedCompetence = competence.Split('-');

				result.Add(new Competence(
					splitedCompetence[0],
					int.Parse(splitedCompetence[1], CultureInfo.InvariantCulture),
					worksheet.Cells[row, descriptionCell.Column].Text));
			}

			return result;
		}
	}
}