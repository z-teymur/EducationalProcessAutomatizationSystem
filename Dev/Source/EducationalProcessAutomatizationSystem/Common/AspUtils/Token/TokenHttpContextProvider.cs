﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace EducationalProcessAutomatizationSystem.Common.AspUtils.Token
{
	public static class TokenHttpContextProvider
	{
		public static string GetToken(HttpContext context) =>
			context.Request.Headers["Authorization"].ToString().Split(' ').Last();

		public static string GetTokenFromCookie(HttpContext context) =>
			context.User.Claims.Single(x => x.Type.Equals("Token", StringComparison.OrdinalIgnoreCase)).Value;
	}
}
