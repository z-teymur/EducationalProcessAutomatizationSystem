﻿using System;
using System.Globalization;

namespace EducationalProcessAutomatizationSystem.Common.AspUtils.Api
{
	public static class ApiUriCreator
	{
		public static Uri Create(string controller, string action)
		{
			return new Uri($"/{controller.ToLower(CultureInfo.InvariantCulture).Replace("controller", string.Empty, true, CultureInfo.InvariantCulture)}/{action.ToLower(CultureInfo.InvariantCulture)}", UriKind.Relative);
		}
	}
}
