﻿using System;
using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes
{
	public sealed class AnonymousOnlyAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			if (context.HttpContext.User.Identity.IsAuthenticated)
			{
				context.Result = new RedirectResult(new Uri($"{SystemRoutingConfiguration.Protocol}://{SystemRoutingConfiguration.Domain}").ToString());
			}
		}
	}
}