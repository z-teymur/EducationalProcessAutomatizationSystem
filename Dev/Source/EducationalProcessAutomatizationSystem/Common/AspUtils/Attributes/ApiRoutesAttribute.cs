﻿using System;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Models;
using EducationalProcessAutomatizationSystem.Common.Utils.Json;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes
{
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class ApiRoutesAttribute : ActionFilterAttribute
	{
		public ApiRoutesAttribute(string section)
		{
			_section = section;
		}

		public override void OnActionExecuting(ActionExecutingContext context)
		{
			var serializer = context.HttpContext.RequestServices.GetService<IJsonSerializer>();
			var apiConfiguration = context.HttpContext.RequestServices.GetService<ApiConfiguration>();
			var routes = apiConfiguration.GetApiRoutes(_section);
			context.HttpContext.Response.Cookies.Append("ApiRoutes", serializer.UseCamelCase().SerializeObject(routes));
		}

		private readonly string _section;
	}
}
