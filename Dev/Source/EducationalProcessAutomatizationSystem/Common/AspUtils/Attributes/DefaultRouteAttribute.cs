﻿using Microsoft.AspNetCore.Mvc;

namespace EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes
{
	public sealed class DefaultRouteAttribute : RouteAttribute
	{
		public DefaultRouteAttribute()
			: base("[controller]/[action]")
		{
		}
	}
}
