﻿using System;
using Microsoft.AspNetCore.Authorization;

namespace EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
	public sealed class RolesAttribute : AuthorizeAttribute
	{
		public RolesAttribute(params string[] roles)
		{
			Roles = string.Join(',', roles);
		}
	}
}
