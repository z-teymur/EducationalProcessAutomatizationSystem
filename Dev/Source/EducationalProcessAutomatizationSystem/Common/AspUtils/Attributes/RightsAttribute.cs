﻿using System;
using System.Linq;
using EducationalProcessAutomatizationSystem.Frontend.Common.Menu;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes
{
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class RightsAttribute : ActionFilterAttribute
	{
		public RightsAttribute(string menu)
		{
			_menu = menu;
		}

		public override void OnActionExecuting(ActionExecutingContext context)
		{
			var hasAccess =
				context.HttpContext.RequestServices.GetService<IFrontendMenu>().Menu
					.Single(mi => mi.Key.Equals(_menu, StringComparison.Ordinal))
					.GetRoles().Any(role => context.HttpContext.User.IsInRole(role));

			if (!hasAccess)
			{
				context.Result = new ForbidResult();
			}
		}

		private readonly string _menu;
	}
}
