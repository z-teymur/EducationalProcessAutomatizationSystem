﻿using System;
using System.Linq;
using EducationalProcessAutomatizationSystem.Common.Utils.Json;
using EducationalProcessAutomatizationSystem.Frontend.Common.Menu;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes
{
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class MenuAttribute : ActionFilterAttribute
	{
		public MenuAttribute(string menu)
		{
			_menu = menu;
		}

		public override void OnActionExecuting(ActionExecutingContext context)
		{
			var menu = context.HttpContext.RequestServices.GetService<IFrontendMenu>();
			var serializer = context.HttpContext.RequestServices.GetService<IJsonSerializer>();

			var allowed = new Func<MenuItem, bool>(mi => mi.GetRoles().Any(role => context.HttpContext.User.IsInRole(role)));
			var hasRequiredKey = new Func<MenuItem, bool>(mi => mi.Key.Equals(_menu, StringComparison.Ordinal));

			var allowedMenu = new FrontendMenu(menu.GlobalMenu.Where(allowed), menu.Menu.Where(allowed));

			var hasAccess = allowedMenu.GlobalMenu.Any(hasRequiredKey) || allowedMenu.Menu.Any(hasRequiredKey);
			if (!hasAccess)
			{
				context.Result = new ForbidResult();
				return;
			}

			allowedMenu.Menu
				.Where(x => x.Key.Equals(_menu, StringComparison.Ordinal))
				.ToList()
				.ForEach(x => x.Select());

			var transformRoute = new Action<MenuItem>(mi =>
			{
				if (!Uri.TryCreate(mi.Route, UriKind.Absolute, out var uri) && context.Controller is Controller controller)
				{
					mi.Route = controller.Url.RouteUrl(mi.Route);
				}
			});

			allowedMenu.Menu.ToList().ForEach(transformRoute);

			context.HttpContext.Response.Cookies.Append("Menu", serializer.UseCamelCase().SerializeObject(allowedMenu));
		}

		private readonly string _menu;
	}
}
