﻿using System;
using System.Collections.Generic;

namespace EducationalProcessAutomatizationSystem.Common.AspUtils.Models
{
	public class ApiConfiguration
	{
		public ApiConfiguration(Dictionary<string, Dictionary<string, Uri>> sectionDictionary)
		{
			_sectionDictionary = sectionDictionary;
		}

		public Dictionary<string, Uri> GetApiRoutes(string section)
		{
			return _sectionDictionary[section];
		}

		private readonly Dictionary<string, Dictionary<string, Uri>> _sectionDictionary;
	}
}
