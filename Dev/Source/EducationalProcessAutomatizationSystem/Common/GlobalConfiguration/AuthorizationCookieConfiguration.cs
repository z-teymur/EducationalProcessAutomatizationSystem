﻿namespace EducationalProcessAutomatizationSystem.Common.GlobalConfiguration
{
	public static class AuthorizationCookieConfiguration
	{
		public static string DataProtectionKeysLocation => "/var/lib/fontend/dataprotectionkeys";

		public static string CookieName => "Auth";
	}
}
