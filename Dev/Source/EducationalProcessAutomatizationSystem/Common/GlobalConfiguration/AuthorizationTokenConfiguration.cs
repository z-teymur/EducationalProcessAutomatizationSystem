﻿namespace EducationalProcessAutomatizationSystem.Common.GlobalConfiguration
{
	public static class AuthorizationTokenConfiguration
	{
		public static string Issuer => nameof(EducationalProcessAutomatizationSystem);

		public static string Audience => "Backend";

		public static string DataProtectionKeysLocation => "/var/lib/backend/dataprotectionkeys";
	}
}
