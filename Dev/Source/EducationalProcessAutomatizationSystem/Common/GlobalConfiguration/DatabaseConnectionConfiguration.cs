﻿using System.Globalization;

namespace EducationalProcessAutomatizationSystem.Common.GlobalConfiguration
{
	public static class DatabaseConnectionConfiguration
	{
		private static string EPASDatabaseReadWriteConnection => "Data Source=host.docker.internal,1433;Initial Catalog=EPASDatabase;User={0};Password=12345";


		public static string GetReadWriteConnection(string login)
		{
			return string.Format(CultureInfo.InvariantCulture, EPASDatabaseReadWriteConnection, login);
		}
	}
}
