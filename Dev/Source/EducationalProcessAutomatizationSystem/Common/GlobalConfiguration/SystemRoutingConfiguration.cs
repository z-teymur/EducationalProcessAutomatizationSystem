﻿using System;

namespace EducationalProcessAutomatizationSystem.Common.GlobalConfiguration
{
	public static class SystemRoutingConfiguration
	{
		public static string Domain => "development.internal";

		public static string ProgrammesManagementSystemDomain => $"prog.{Domain}";

		public static string Protocol => "http";

		public static Uri MainServiceApiUri => new Uri($"{Protocol}://mainservice:80");

		public static Uri ProgrammesManagementSystemServiceApiUri => new Uri($"{Protocol}://programmesmanagementsystemservice:80");

		public static Uri ElasticSearchUri => new Uri($"{Protocol}://elasticsearch:9200/");
	}
}
