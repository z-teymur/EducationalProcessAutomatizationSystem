﻿using System;
using System.Globalization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace EducationalProcessAutomatizationSystem.Common.Extensions.Routing
{
	public static class RoutingExtensions
	{
		public static void MapControllerAbsoluteRoute(
			this IEndpointRouteBuilder endpoint,
			string route,
			string controller,
			string action)
		{
			controller = GetControllerName(controller);

			endpoint.MapControllerRoute(
				route,
				route,
				new { controller, action });
		}

		public static string GetControllerName(string controller)
		{
			return controller
				.ToLower(CultureInfo.InvariantCulture)
				.Replace("controller", string.Empty, StringComparison.InvariantCulture);
		}
	}
}