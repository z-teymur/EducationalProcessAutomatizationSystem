﻿using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Sinks.Elasticsearch;

namespace EducationalProcessAutomatizationSystem.Common.Extensions.Logging
{
	public static class ElasticLoggerExtension
	{
		public static void AddElasticLogging(this IApplicationBuilder app, ILoggerFactory loggerFactory)
		{
			var elasticUri = SystemRoutingConfiguration.ElasticSearchUri;

			Log.Logger = new LoggerConfiguration()
			.Enrich.FromLogContext()
			.WriteTo.Elasticsearch(new ElasticsearchSinkOptions(elasticUri)
			{
				AutoRegisterTemplate = true,
			})
			.CreateLogger();
			loggerFactory.AddSerilog();
		}
	}
}
