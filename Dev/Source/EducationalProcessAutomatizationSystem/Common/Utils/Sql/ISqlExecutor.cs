﻿using System.Collections.Generic;

namespace EducationalProcessAutomatizationSystem.Common.Utils.Sql
{
	public interface ISqlExecutor
	{
		TEntity GetSingle<TEntity>(string storedProcedure, object parameters);

		IEnumerable<TEntity> GetMany<TEntity>(string storedProcedure, object parameters = null);

		void Execute(string storedProcedure, object parameters);
	}
}
