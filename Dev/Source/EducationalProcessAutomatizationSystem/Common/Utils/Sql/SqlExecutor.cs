﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;

namespace EducationalProcessAutomatizationSystem.Common.Utils.Sql
{
	public sealed class SqlExecutor : ISqlExecutor, IDisposable
	{
		public void Dispose()
		{
			_connection.Dispose();
		}

		public SqlExecutor(string connection)
		{
			_connection = new SqlConnection(connection);
		}

		public TEntity GetSingle<TEntity>(string storedProcedure, object parameters)
		{
			return _connection.QuerySingleOrDefault<TEntity>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
		}

		public IEnumerable<TEntity> GetMany<TEntity>(string storedProcedure, object parameters = null)
		{
			return _connection.Query<TEntity>(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
		}

		public void Execute(string storedProcedure, object parameters)
		{
			_connection.Execute(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
		}

		private readonly IDbConnection _connection;
	}
}
