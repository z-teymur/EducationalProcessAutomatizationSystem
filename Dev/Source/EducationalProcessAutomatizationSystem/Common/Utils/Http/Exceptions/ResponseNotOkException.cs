﻿using System.Net.Http;

namespace EducationalProcessAutomatizationSystem.Common.Utils.Http.Exceptions
{
	public class ResponseNotOkException : HttpRequestException
	{
		public ResponseNotOkException()
		{
		}
	}
}
