﻿using System.Collections.Generic;
using System.Net.Http.Headers;

namespace EducationalProcessAutomatizationSystem.Common.Utils.Http
{
	public class HttpExecutionResult<TData>
	{
		public Dictionary<string, object> Headers { get; private set; }

		public TData Result { get; private set; }

		public HttpExecutionResult(Dictionary<string, object> headers, TData result)
		{
			Result = result;
			Headers = headers;
		}
	}
}
