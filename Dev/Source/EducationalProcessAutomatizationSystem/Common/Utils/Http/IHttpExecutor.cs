﻿using System;

namespace EducationalProcessAutomatizationSystem.Common.Utils.Http
{
	public interface IHttpExecutor
	{
		HttpExecutionResult<TOutput> Post<TInput, TOutput>(Uri address, TInput data)
			where TOutput : new();

		HttpExecutionResult<TOutput> Get<TOutput>(Uri address)
			where TOutput : new();

		HttpExecutionResult<TOutput> Get<TOutput>(Uri address, params (string name, string value)[] data)
			where TOutput : new();

		HttpExecutor WithBearerJwt(string token);
	}
}