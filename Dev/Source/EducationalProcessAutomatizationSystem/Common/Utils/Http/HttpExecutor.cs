﻿using System;
using System.Linq;
using System.Net;
using EducationalProcessAutomatizationSystem.Common.Utils.Http.Exceptions;
using RestSharp;
using RestSharp.Authenticators;

namespace EducationalProcessAutomatizationSystem.Common.Utils.Http
{
	public sealed class HttpExecutor : IHttpExecutor
	{
		public HttpExecutor()
		{
			_client = new RestClient();
		}

		public HttpExecutor WithBearerJwt(string token)
		{
			_client.Authenticator = new JwtAuthenticator(token);

			return this;
		}

		public HttpExecutionResult<TOutput> Post<TInput, TOutput>(Uri address, TInput data)
			where TOutput : new()
		{
			var response = _client.Post<TOutput>(new RestRequest(address).AddJsonBody(data));

			if (!IsStatusCodeAcceptable(response.StatusCode))
			{
				throw new ResponseNotOkException();
			}

			return new HttpExecutionResult<TOutput>(response.Headers.ToDictionary(x => x.Name, y => y.Value), response.Data);
		}

		public HttpExecutionResult<TOutput> Get<TOutput>(Uri address)
			where TOutput : new()
		{
			var response = _client.Get<TOutput>(new RestRequest(address));

			if (!IsStatusCodeAcceptable(response.StatusCode))
			{
				throw new ResponseNotOkException();
			}

			return new HttpExecutionResult<TOutput>(response.Headers.ToDictionary(x => x.Name, y => y.Value), response.Data);
		}

		public HttpExecutionResult<TOutput> Get<TOutput>(Uri address, params (string name, string value)[] data)
			where TOutput : new()
		{
			var request = new RestRequest(address);
			data.ToList().ForEach(x => request.AddQueryParameter(x.name, x.value));

			var response = _client.Get<TOutput>(request);

			if (!IsStatusCodeAcceptable(response.StatusCode))
			{
				throw new ResponseNotOkException();
			}

			return new HttpExecutionResult<TOutput>(response.Headers.ToDictionary(x => x.Name, y => y.Value), response.Data);
		}

		private bool IsStatusCodeAcceptable(HttpStatusCode statusCode)
		{
			return (int)statusCode >= 200 && (int)statusCode < 300;
		}


		private readonly RestClient _client;
	}
}
