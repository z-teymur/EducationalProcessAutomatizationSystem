﻿namespace EducationalProcessAutomatizationSystem.Common.Utils.Json
{
	public interface IJsonSerializer
	{
		string SerializeObject<TObject>(TObject o);

		IJsonSerializer UseCamelCase();

		TObject DeserializeObject<TObject>(string json);
	}
}
