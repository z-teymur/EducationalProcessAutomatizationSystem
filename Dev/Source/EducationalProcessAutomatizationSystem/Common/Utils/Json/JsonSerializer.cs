﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace EducationalProcessAutomatizationSystem.Common.Utils.Json
{
	public class JsonSerializer : IJsonSerializer
	{
		public string SerializeObject<TObject>(TObject o)
		{
			return JsonConvert.SerializeObject(o, _settings);
		}

		public IJsonSerializer UseCamelCase()
		{
			_settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			return this;
		}

		public TObject DeserializeObject<TObject>(string json)
		{
			return JsonConvert.DeserializeObject<TObject>(json, _settings);
		}

		private readonly JsonSerializerSettings _settings = new JsonSerializerSettings();
	}
}
