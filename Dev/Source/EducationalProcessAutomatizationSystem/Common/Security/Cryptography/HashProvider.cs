﻿using System.Globalization;
using System.Linq;
using System.Security.Cryptography;

namespace EducationalProcessAutomatizationSystem.Common.Security.Cryptography
{
	public class HashProvider : IHashProvider
	{
		public string GetSha512(string text)
		{
			var bytes = System.Text.Encoding.UTF8.GetBytes(text);

			using var hash = SHA512.Create();
			var hashedInputBytes = hash.ComputeHash(bytes);
			return GetString(hashedInputBytes);
		}

		public byte[] GetSha256(byte[] bytes)
		{
			using var hash = SHA256.Create();
			return hash.ComputeHash(bytes);
		}

		private string GetString(byte[] bytes)
		{
			var stringBuilder = new System.Text.StringBuilder(bytes.Length);
			bytes.ToList().ForEach(@byte => stringBuilder.Append(@byte.ToString("X2", CultureInfo.InvariantCulture)));
			return stringBuilder.ToString();
		}
	}
}
