﻿namespace EducationalProcessAutomatizationSystem.Common.Security.Cryptography
{
	public interface IHashProvider
	{
		string GetSha512(string text);

		byte[] GetSha256(byte[] bytes);
	}
}
