﻿namespace EducationalProcessAutomatizationSystem.Common.CommonModels
{
	public interface IServiceInteractor
	{
		void UseBearerJwt(string token);
	}
}