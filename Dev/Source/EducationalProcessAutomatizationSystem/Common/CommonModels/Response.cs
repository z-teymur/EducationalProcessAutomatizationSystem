﻿namespace EducationalProcessAutomatizationSystem.Common.CommonModels
{
	public class Response<TData>
	{
		public TData Data { get; set; }

		public string Message { get; set; }

		public bool IsError { get; set; }

		public Response()
		{
			IsError = false;
		}

		public Response(TData data)
		{
			Data = data;
			IsError = false;
		}

		public Response(TData data, string message)
		{
			Data = data;
			Message = message;
			IsError = false;
		}

		public Response(string message)
		{
			Message = message;
			IsError = true;
		}
	}
}
