var ExtractTextPlugin = require("extract-text-webpack-plugin");
var glob = require("glob");
var path = require("path");

var currentDirrectory = path.join(__dirname, "../");


var trimExtension = fileName => /(.+?)(\.[^.]*$|$)/.exec(fileName)[1];
var outputFileTransform = filePath => filePath.replace("wwwroot/", "wwwroot/build/")


var output = "../"
var entrypoints = "/**/*.{main.tsx,less}"
var paths = [
	...glob.sync("./" + path.join("Frontend/Main/wwwroot/", entrypoints)),
	...glob.sync("./" + path.join("Frontend/ProgrammesManagementSystem/wwwroot/", entrypoints))
]



var entryObject = {};
paths.map(x => entryObject[outputFileTransform(trimExtension(x))] = x );

console.log(entryObject) 


var extractLess = new ExtractTextPlugin("[name].css");
module.exports = {
	mode: "development",
	devtool: "source-map",
	resolve: {
		alias: {
			modulespackage: path.resolve(__dirname, "../Frontend/Common/ModulesPackage")
		},
		extensions: [".ts", ".tsx", ".js"]
	},
	entry: entryObject,
    output: {
      path: path.join(__dirname, output),
    },
	module: {
		rules: [
			{
				test: /\.ts(x?)$/,
				exclude: /node_modules/,
				use: [
					{ loader: "ts-loader" }
				]
			},
			{
				test: /\.css$/i,
				use: ['css-loader']
			},
			{
			test: /\.less/,
			use: extractLess.extract({
				use: [
					{ loader: "css-loader" },
					{ loader: "less-loader" }
				]
			})
		}]
	},
	plugins: [
		extractLess
	]
};