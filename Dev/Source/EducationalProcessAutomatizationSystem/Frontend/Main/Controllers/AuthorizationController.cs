﻿using System;
using EducationalProcessAutomatizationSystem.Backend.MainService.Contract;
using EducationalProcessAutomatizationSystem.Backend.MainService.Interactor;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Common.CommonModels;
using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using EducationalProcessAutomatizationSystem.Common.Security.Cryptography;
using EducationalProcessAutomatizationSystem.Frontend.Common.CookieControl;
using EducationalProcessAutomatizationSystem.Frontend.Main.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EducationalProcessAutomatizationSystem.Frontend.Main.Controllers
{
	public class AuthorizationController : Controller
	{
		private readonly ILogger<AuthorizationController> _logger;
		private readonly IAuthorizer _authorizer;
		private readonly IHashProvider _hashProvider;
		private readonly IMainServiceInteractor _mainServiceInteractor;

		public AuthorizationController(
			IAuthorizer authorizer,
			IMainServiceInteractor mainServiceInteractor,
			IHashProvider hashProvider,
			ILogger<AuthorizationController> logger)
		{
			_logger = logger;
			_authorizer = authorizer;
			_hashProvider = hashProvider;
			_mainServiceInteractor = mainServiceInteractor;
		}


		[AllowAnonymous]
		[AnonymousOnly]
		[HttpGet]
		public IActionResult Login()
		{
			return View();
		}

		[AllowAnonymous]
		[AnonymousOnly]
		[HttpPost]
		public Response<object> Login(string login, string password)
		{
			using var c = _logger.BeginScope(nameof(Login));

			if (login == null && password == null)
			{
				return new Response<object>(AuthorizationResources.NoCredentialsProvided);
			}

			if (string.IsNullOrEmpty(login?.Trim()))
			{
				return new Response<object>(AuthorizationResources.NoLoginProvided);
			}

			if (string.IsNullOrEmpty(password?.Trim()))
			{
				return new Response<object>(AuthorizationResources.NoPasswordProvided);
			}

			_logger.LogInformation($"Received correct login credentials: {login}");

			var loginCredentials = new LoginCredentials
			{
				Login = login,
				Password = _hashProvider.GetSha512(password)
			};

			_logger.LogInformation("Requesting MainService for user data");
			var response = _mainServiceInteractor.Login(loginCredentials, out bool passwordUpdateRequired);

			if (response.IsError)
			{
				_logger.LogInformation($"Response came with error {response.Message}");
				return new Response<object>(response.Message);
			}

			if (passwordUpdateRequired)
			{
				return new Response<object>(new { PasswordUpdateRequired = true });
			}

			_logger.LogInformation($"Creating cookie");
			_authorizer.CreateCookieAsync(HttpContext, response.Data)
				.ConfigureAwait(false)
				.GetAwaiter()
				.GetResult();

			return new Response<object>(
				new
				{
					Redirect = new Uri($"{SystemRoutingConfiguration.Protocol}://{SystemRoutingConfiguration.Domain}").ToString()
				});
		}

		[AllowAnonymous]
		[HttpGet]
		public IActionResult ChangePassword(string passwordChangeToken)
		{
			passwordChangeToken.GetType();
			return View();
		}
	}
}
