﻿using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Frontend.Main.Resources;
using Microsoft.AspNetCore.Mvc;

namespace EducationalProcessAutomatizationSystem.Frontend.Main.Controllers
{
	public class HomeController : Controller
	{
		[HttpGet]
		[Menu(nameof(MenuResources.HomePage))]
		public IActionResult Index() => View();
	}
}
