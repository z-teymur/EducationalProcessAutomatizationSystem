﻿import "react-app-polyfill/ie11"
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as Yup from "yup";

import axios from "axios";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import Alert from "@material-ui/lab/Alert";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/core/styles";

import { Formik, Form, Field } from "formik";

import theme from "modulespackage/MaterialUi/Theme";
import { FormDataExtensions } from "modulespackage/Helpers/FormDataExtensions";
import { TextFieldValidation } from "modulespackage/Helpers/FormikValidation";

class LoginPage extends React.Component<{}, any> {
	constructor(props) {
		super(props);
		this.state = {
			alert: { visible: false, severity: null, message: null }
		}

		this.formValidationSchema = Yup.object().shape({
			login: Yup.string().required('Введите логин'),
			password: Yup.string().required('Введите пароль')
		});
	}
	private formValidationSchema;


	private showError = (message: string) => {
		this.setState({ alert: { visible: true, severity: "error", message: message } });
	};

	private showSuccess = (message: string) => {
		this.setState({ alert: { visible: true, severity: "success", message: message } });
	};

	private dropAlert = () => {
		this.state.alert.visible = false;
		this.setState(this.state);
	}

	render() {
		return (
			<Formik
				initialValues={{ login: "", password: "" }}
				validationSchema={this.formValidationSchema}
				onSubmit={this.onSubmit}>
				{({ errors, touched, isSubmitting }) => {
					let textFieldValidation = new TextFieldValidation(errors, touched);
					return (
						<Grid
							container
							spacing={0}
							alignItems="center"
							justify="center"
							style={{ minHeight: '100vh' }}>
							<Grid item lg={4} xl={4}>
								<Typography align="center" variant="h3">Вход в систему</Typography>
								{this.state.alert.visible &&
									<Alert severity={this.state.alert.severity}>{this.state.alert.message}</Alert>
								}
								<Form>
									<Field
										label="Логин"
										name="login"
										as={TextField}
										{...textFieldValidation.validate("login")}
										inputProps={{ readOnly: isSubmitting, onChange: this.dropAlert }} />
									<Field
										label="Пароль"
										name="password"
										as={TextField}
										{...textFieldValidation.validate("password")}
										inputProps={{ readOnly: isSubmitting, onChange: this.dropAlert }}
										type="password" />
									{isSubmitting
										? <Container style={{ textAlign: "center" }}><CircularProgress size={24} /></Container>
										: <Button type="submit" onClick={this.dropAlert}>Вход</Button>
									}
								</Form>
							</Grid>
						</Grid>
					)
				}
				}
			</Formik>
		);
	}

	private onSubmit = async (values, actions) => {
		await axios
			.post('', FormDataExtensions.fromObject(values))
			.then(response => {
				var responsePayload = response.data;
				if (responsePayload.isError)
					this.showError(responsePayload.message);
				else {
					this.showSuccess("Вход успешен");

					if (responsePayload.data.redirect) {
						window.location.replace(responsePayload.data.redirect);
					} else {
						actions.setSubmitting(false);
					}
				}
				console.log(responsePayload);
			})
			.catch(() => {
				this.showError("Возникла ошибка");
				actions.setSubmitting(false);
			});
	}
}


ReactDOM.render(
	<ThemeProvider theme={createMuiTheme(theme)}>
		<LoginPage />
	</ThemeProvider>,
	document.getElementById("root")
);