﻿using Microsoft.Extensions.DependencyInjection;

namespace EducationalProcessAutomatizationSystem.Frontend.Main
{
	public static class Bootstrapper
	{
		public static void RegisterDependencies(this IServiceCollection services)
		{
			services.AddScoped(provider => MenuComposer.Compose());
		}
	}
}
