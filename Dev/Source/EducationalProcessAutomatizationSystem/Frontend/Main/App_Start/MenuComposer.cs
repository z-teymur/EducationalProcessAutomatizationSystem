﻿using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Constants;
using EducationalProcessAutomatizationSystem.Frontend.Common.Menu;
using EducationalProcessAutomatizationSystem.Frontend.Main.Resources;

namespace EducationalProcessAutomatizationSystem.Frontend.Main
{
	public static class MenuComposer
	{
		public static IFrontendMenu Compose()
		{
			var globalMenu = new GlobalMenuFactory().GetGlobalMenu();

			var menu = new List<MenuItem>
			{
				new MenuItem(
					"/Menu/menu-home.svg",
					MenuResources.HomePage,
					nameof(MenuResources.HomePage),
					"/",
					Roles.Everybody)
			};

			return new FrontendMenu(globalMenu, menu);
		}
	}
}
