﻿using EducationalProcessAutomatizationSystem.Common.Extensions.Routing;
using EducationalProcessAutomatizationSystem.Frontend.Main.Controllers;
using Microsoft.AspNetCore.Routing;

namespace EducationalProcessAutomatizationSystem.Frontend.Main
{
	public static class RouteConfig
	{
		public static void Configure(IEndpointRouteBuilder routes)
		{
			routes.MapControllerAbsoluteRoute(
				"Login",
				nameof(AuthorizationController),
				nameof(AuthorizationController.Login));

			routes.MapControllerAbsoluteRoute(
				"ChangePassword",
				nameof(AuthorizationController),
				nameof(AuthorizationController.ChangePassword));

			routes.MapControllerAbsoluteRoute(
				"/",
				nameof(HomeController),
				nameof(HomeController.Index));
		}
	}
}
