"use strict";
exports.__esModule = true;
require("npm-bridge/react-app-polyfill/ie11.b");
var React = require("npm-bridge/react.b");
var ReactDOM = require("npm-bridge/react-dom.b");
var Layout_1 = require("modulespackage/Components/Layout");
var Typography_b_1 = require("npm-bridge/@material-ui/core/Typography.b");
var styles_b_1 = require("npm-bridge/@material-ui/core/styles.b");
var Theme_1 = require("modulespackage/MaterialUi/Theme");
ReactDOM.render(React.createElement(styles_b_1.ThemeProvider, { theme: styles_b_1.createMuiTheme(Theme_1["default"]) },
    React.createElement(Layout_1["default"], null,
        React.createElement(Typography_b_1["default"], { paragraph: true }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Rhoncus dolor purus non enim praesent elementum facilisis leo vel. Risus at ultrices mi tempus imperdiet. Semper risus in hendrerit gravida rutrum quisque non tellus. Convallis convallis tellus id interdum velit laoreet id donec ultrices. Odio morbi quis commodo odio aenean sed adipiscing. Amet nisl suscipit adipiscing bibendum est ultricies integer quis. Cursus euismod quis viverra nibh cras. Metus vulputate eu scelerisque felis imperdiet proin fermentum leo. Mauris commodo quis imperdiet massa tincidunt. Cras tincidunt lobortis feugiat vivamus at augue. At augue eget arcu dictum varius duis at consectetur lorem. Velit sed ullamcorper morbi tincidunt. Lorem donec massa sapien faucibus et molestie ac."),
        React.createElement(Typography_b_1["default"], { paragraph: true }, "Consequat mauris nunc congue nisi vitae suscipit. Fringilla est ullamcorper eget nulla facilisi etiam dignissim diam. Pulvinar elementum integer enim neque volutpat ac tincidunt. Ornare suspendisse sed nisi lacus sed viverra tellus. Purus sit amet volutpat consequat mauris. Elementum eu facilisis sed odio morbi. Euismod lacinia at quis risus sed vulputate  odio. Morbi tincidunt ornare massa eget egestas purus viverra accumsan in. In hendrerit gravida rutru m quisque non tellus orci ac. Pellentesque nec nam aliquam sem et tortor. Habitant morbi tristique sene ctus et. Adipiscing elit duis tristique sollicitudin nibh sit. Ornare aenean euismod elementum nisi quis eleifend. Commodo viverra maecenas accumsan lacus vel facilisis. Nulla posuere sollicitudin aliquam ultrices sagittis orci a."))), document.getElementById("root"));
//# sourceMappingURL=Index.main.js.map