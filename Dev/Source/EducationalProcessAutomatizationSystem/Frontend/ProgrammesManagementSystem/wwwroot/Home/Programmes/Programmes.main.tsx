﻿import "react-app-polyfill/ie11"
import * as React from "react";
import * as ReactDOM from "react-dom";

import Layout from "modulespackage/Components/Layout";
import SystemConstants from "modulespackage/Constants/SystemConstants";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import EditIcon from '@material-ui/icons/Edit';
import GetAppIcon from '@material-ui/icons/GetApp';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


import CircularProgress from "@material-ui/core/CircularProgress";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import theme from "modulespackage/MaterialUi/Theme";
import axios from "axios";
import * as Cookies from "js-cookie";

interface IDisciplineList {
	specialityCode: string;
	specialityName: string;
	profileName: string;
	disciplineName: string;
	disciplineUid: string;
	programmeUid: string;
};

interface IProgrammesState{
	disciplineList : Array<IDisciplineList>;
	loading: boolean;
}

class Programmes extends React.Component<any, IProgrammesState> {
	constructor(props) {
		super(props);

		this.routingData = Cookies.getJSON(SystemConstants.apiCookieName);

		this.state = {
			loading: true,
			disciplineList: null
		}
	}

	private routingData;

	
	componentDidMount() {
		let self = this;
		axios
			.get(this.routingData.getProgrammes)
			.then(responce => {
				self.setState({ disciplineList: responce.data.data });
			})
			.finally(() => {
				self.setState({ loading: false })
			})
	}

	render() {
		return this.state.loading 
			? (
				<Box display="flex" height={1} justifyContent="center" alignItems="center">
					<CircularProgress size={40} />
				</Box>
			) 
			: this.state.disciplineList.length === 0 
			?  (
				<Box>
					<Typography variant="h5" align="center">Для Вас не найдено дисциплин</Typography>
				</Box>
			)
			: (
				<Box>
					<TableContainer component={Paper}>
						<Table size="small">
							<TableHead>
								<TableRow>
									<TableCell>Специальность</TableCell>
									<TableCell>Профиль</TableCell>
									<TableCell>Дисциплина</TableCell>
									<TableCell></TableCell>
									<TableCell></TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{this.state.disciplineList.map(d => (
								<TableRow key={d.specialityName + d.disciplineName}>
									<TableCell>{d.specialityCode + d.specialityName}</TableCell>
									<TableCell>{d.profileName}</TableCell>
									<TableCell>{d.disciplineName}</TableCell>
									<TableCell>
										<a href={this.routingData.edit + "?disciplineUid=" + d.disciplineUid + "&uid=" + d.programmeUid}><EditIcon color="primary"/></a>
									</TableCell>
									<TableCell>
										{d.programmeUid === null 
											? <GetAppIcon color="disabled" />
											: <a href={this.routingData.download+ "?uid="+d.programmeUid}><GetAppIcon color="primary" /></a>
										}
									</TableCell>
								</TableRow>
							))}
							</TableBody>
						</Table>
					</TableContainer>
				</Box>
			)
	}
}


ReactDOM.render(
	<ThemeProvider theme={createMuiTheme(theme)}>
		<Layout>
			<Programmes />
		</Layout>
	</ThemeProvider>,
	document.getElementById("root")
);