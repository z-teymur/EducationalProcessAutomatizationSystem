﻿import "react-app-polyfill/ie11"
import * as React from "react";
import * as ReactDOM from "react-dom";

import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Input from "@material-ui/core/Input";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

import Grid from "@material-ui/core/Grid";

import JoditEditor from "jodit-react";

import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import theme from "modulespackage/MaterialUi/Theme";
import SimpleExpansionPanel from "modulespackage/Components/SimpleExpansionPanel";
import ComplexList from "modulespackage/Components/ComplexList";
import SimpleSelect from "modulespackage/Components/SimpleSelect";
import StateBlock from "modulespackage/Components/StateBlock";
import StringHelper from "modulespackage/Helpers/StringHelper";
import Numeral from "modulespackage/Grammar/Numeral";
import Guid from "modulespackage/Helpers/Guid";


class ValuationFund extends React.Component<any, any> {
	constructor(props) {
		super(props);

		this.state = {
			document: {
				year: 2020,
				disciplineName: "Базы Данных",
				specialityCode: "09.03.02",
				specialityName: "Информационные системы и технологии",
				profileName: "Программное обеспечение игровой компьютерной индустрии",
				trainingFormName: "Очная",
				typesOfProfessionalActivity: ["научно-исследовательская", "проектно-технологическая"],
				deartament: "Информатика и информационные технологии",
				creators: ["Фамилия И.О."],
				expectedTrainingResults: {
					"961bf855-7104-45ef-be9c-a7a7bdab9a3e" : {
						competenceCode: "ПК-11",
						competenceDescription: "способностью к проектированию ...",
						trainingResults: { know: "то, что нужно знать", can: "то, что нужно уметь", able: "то, чем надо владеть" }
					},
					"5025d933-b1a2-4b6b-a437-c603b04705e6" : {
						competenceCode: "ПК-12",
						competenceDescription: "способностью к проектированию ...",
						trainingResults: { know: "то, что нужно знать2", can: "то, что нужно умет2", able: "то, чем надо владеть 2" },
					}
				},

				chapter_1: {
					table: [
						{ 
							expectedTrainingResultUid: "961bf855-7104-45ef-be9c-a7a7bdab9a3e",
							competenceBuildingTechnologies: "лекция, самостоятельная работа..., ещё чтото",
							valuationForm: "Л, К/Р",
							competencelearningDegrees: {
								baseLevel: [],
								elevatedLevel: []
							}
						},
						{ 
							expectedTrainingResultUid:"5025d933-b1a2-4b6b-a437-c603b04705e6",
							competenceBuildingTechnologies: "лекция, самостоятельная работа..., ещё чтото 2",
							valuationForm: "Л, К/Р ///",
							competencelearningDegrees: {
								baseLevel: [],
								elevatedLevel: []
							}
						}
					]
				},

				chapter_2:{
					valuationTools: [
						{
							name: "Лабораторные работы",
							description: "Средство контроля усвоения учебного материала",
							descriptionInFOS: "Задания к лабораторным работам"
						},
						{
							name: "Курсовой проект",
							description: "Средство контроля усвоения учебного материала и написание курсового проекта",
							descriptionInFOS: "Задания к курсовым работам"
						}

					],

					subchapters: [
						{
							title: "Воспросы к экзамену"
						}


					]
				}

			},
			expandedPanelUid: undefined,
			stateUpdateSignal: "-"
		}
	}

	private onExpansionPanelChange = (expandedUid: string) => {
		this.setState(state => ({ ...state, expandedPanelUid: expandedUid }));
	};


	private richEditorButtonsConfigurationButtons = [ 'undo', 'redo', '|', 'bold', 'strikethrough', 'underline', 'italic', '|', 'superscript', 'subscript', '|', 'ul', 'ol', '|', '\n', 'selectall', 'cut', 'copy', 'paste', '|', 'fullsize', 'print' ];
	private richEditorButtonsConfiguration = {
		readonly:false,
		buttons: this.richEditorButtonsConfigurationButtons,
		buttonsMD: this.richEditorButtonsConfigurationButtons,
		buttonsSM: this.richEditorButtonsConfigurationButtons,
		buttonsXS: this.richEditorButtonsConfigurationButtons,
	};

	render() {
		const document = this.state.document;
		const ePUid = this.state.expandedPanelUid;


		return (
			<div> 
				<Typography paragraph variant="h5" align="center">
					Форнд оценочных средств <i>дисциплины</i>
				</Typography>
				<Box>
					<Typography variant="subtitle1">
						Дисциплина: <b>{document.disciplineName}</b>
					</Typography>
					<Typography variant="subtitle1">
						Направление подготовки: <b>{document.specialityCode} {document.specialityName}</b>
					</Typography>
					<Typography variant="subtitle1">
						Профиль: <b>{document.profileName}</b>
					</Typography>
					<Typography variant="subtitle1">
						Вид профессиональной деятельности: <b>{document.typesOfProfessionalActivity.join("; ")}</b>
					</Typography>
					<Typography variant="subtitle1">
						Кафедра: <b>{document.deartament}</b>
					</Typography>
					<Typography variant="subtitle1">
						Составители: <b>{document.creators.join("; ")}</b>
					</Typography>
					<Typography variant="subtitle1">
						Год: <b>{document.year}</b>
					</Typography>
				</Box>
				<Box>
					<SimpleExpansionPanel header="1. Паспорт фонда оценочных средств" expanded={ePUid} onChange={this.onExpansionPanelChange}>
						{document.chapter_1.table.map(tableRow=> (
							<Grid key={document.expectedTrainingResults[tableRow.expectedTrainingResultUid].competenceCode} container spacing={1}>
								<Grid container item xs={12} sm={1}>
									<Box overflow="hidden">
										<Typography>{document.expectedTrainingResults[tableRow.expectedTrainingResultUid].competenceCode}</Typography>
									</Box>
								</Grid>
								<Grid container item xs={12} sm={2}>
									<Box overflow="hidden">
										<Typography>{document.expectedTrainingResults[tableRow.expectedTrainingResultUid].competenceDescription}</Typography>
									</Box>
								</Grid>
								<Grid container item xs={12} sm={2}>
									<Box overflow="hidden">
										<Typography>
											<b>знать:</b>{document.expectedTrainingResults[tableRow.expectedTrainingResultUid].trainingResults.know}<br />
											<b>уметь:</b>{document.expectedTrainingResults[tableRow.expectedTrainingResultUid].trainingResults.can}<br />
											<b>владеть:</b>{document.expectedTrainingResults[tableRow.expectedTrainingResultUid].trainingResults.able}<br />
										</Typography>
									</Box>
								</Grid>
								<Grid container item xs={12} sm={2}>
									<Box overflow="hidden">
										<Typography>{tableRow.competenceBuildingTechnologies}</Typography>
									</Box>
								</Grid>
								<Grid container item xs={12} sm={1}>
									<Box overflow="hidden">
										<Typography>{tableRow.valuationForm}</Typography>
									</Box>
								</Grid>
								<Grid container item xs={12} sm={4}>
									<Typography display="block"><b>Базовый уровень</b></Typography>
									<ComplexList collection={tableRow.competencelearningDegrees.baseLevel} isBasicTyped empty={""} onCollectionChanged={(newCollection) => { tableRow.competencelearningDegrees.baseLevel = newCollection; this.setState(state => state)}}>
										{(item: any, itemUpdate: Function, entryListUpdate: Function) => (
											<Input fullWidth value={item.value} onChange={event => { item.value = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
										)}
									</ComplexList>
									<br />
									<Typography display="block"><b>Повышенный уровень</b></Typography>
									<ComplexList collection={tableRow.competencelearningDegrees.elevatedLevel} isBasicTyped empty={""} onCollectionChanged={(newCollection) => { tableRow.competencelearningDegrees.elevatedLevel = newCollection; this.setState(state => state)}}>
										{(item: any, itemUpdate: Function, entryListUpdate: Function) => (
											<Input fullWidth value={item.value} onChange={event => { item.value = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
										)}
									</ComplexList>
								</Grid>
							</Grid>
						))}
					</SimpleExpansionPanel>
					<SimpleExpansionPanel header="2. Описание оценочных средств" expanded={ePUid} onChange={this.onExpansionPanelChange}>
						Нужно уточнить, это автоматически генерируется или вручную? Или должна быть возможность выбора из списка? 
						Или из той самой таблицы оно достаётся в зависимоти от количества часов? 
						<TableContainer component={Paper}>
							<Table size="small">
								<TableHead>
								<TableRow>
									<TableCell>№ ОС</TableCell>
									<TableCell>Наименование оценочного средства</TableCell>
									<TableCell>Краткая харектеристика оценочного средства</TableCell>
									<TableCell>Представление оценосного средства в ФОС</TableCell>
								</TableRow>
								</TableHead>
								<TableBody>
								{document.chapter_2.valuationTools.map((vt, index) => (
									<TableRow key={vt.name}>
										<TableCell>{index + 1}</TableCell>
										<TableCell>{vt.name}</TableCell>
										<TableCell>{vt.description}</TableCell>
										<TableCell>{vt.descriptionInFOS}</TableCell>
									</TableRow>
								))}
								</TableBody>
							</Table>
						</TableContainer>
					</SimpleExpansionPanel>

				</Box>

				<button onClick={e => this.setState(state => ({...state, stateUpdateSignal: Guid.get()}))}>Ok</button>
			</div>
		);
	}
}


ReactDOM.render(
	<ThemeProvider theme={createMuiTheme(theme)}>
		<ValuationFund/>
	</ThemeProvider>,
	document.getElementById("root")
);