interface ICompetence {
	code: string;
	description: string;
}

interface IAdditionalInfo
{
	year: number;
	disciplineName: string;
	specialityCode: string;
	specialityName: string;
	profileName: string;
	qualificationName: string;
	trainingFormName: string;
}

interface ITimeSchedule
{
	term: number;
	lectures: number;
	seminars: number;
	practices: number;
	laboratorys: number;
	individuals: number;
	controlls: number;
	interactives: number;
	exams: number;
	credits: number;
}

interface ITrainingResult {
	know: string;
	can: string;
	able: string;
}

interface IDocument {
	chapter_1: {
		plainHtml: string;
		targets: Array<string>;
		goals: Array<string>;
	},

	chapter_2: {
		contentHeader: string;
		disciplinesAfter: Array<string>;
		contentMiddle: string;
		disciplinesBefore: Array<string>;
	},

	chapter_3: {
		competenceTable: Array<{ competenceCode: string, trainingResult: ITrainingResult }>
	},

	chapter_4: {
		disciplineOverallInfo: string;
		disciplineSectionsContent: Array<{ term: number,sectionContent: Array<{header: string, content:string }>}>
	},

	chapter_5: {
		content: string;
	},

	chapter_6: {
		contentHeader: string,
		termsPlan: Array<{ term: number, sectionContent: Array<string>}>,
		contentFooter: string,
	}
}


interface IProgrammeState {
	loading: boolean
	repertories: {
		competences: Array<ICompetence>
		disciplines: Array<string>
	},
	additionalInformation: IAdditionalInfo
	timeSchedule: Array<ITimeSchedule>
	document: IDocument

	expandedPanelUid: string
	stateUpdateSignal: string
	chapter_1_plainHtmlSwitch: boolean

	savingInProgress: boolean
	error: {
		is: boolean
		message: string
	}
}

interface IProgrammeContract {
	uid: string; 
	content: string;
	rowVersion: string;
}