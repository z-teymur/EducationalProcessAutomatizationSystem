﻿import "react-app-polyfill/ie11"
import * as React from "react";
import * as ReactDOM from "react-dom";

import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Input from "@material-ui/core/Input";
import Alert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import CircularProgress from "@material-ui/core/CircularProgress";

import JoditEditor from "jodit-react";

import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import theme from "modulespackage/MaterialUi/Theme";
import SimpleExpansionPanel from "modulespackage/Components/SimpleExpansionPanel";
import ComplexList from "modulespackage/Components/ComplexList";
import SimpleSelect from "modulespackage/Components/SimpleSelect";
import StateBlock from "modulespackage/Components/StateBlock";
import StringHelper from "modulespackage/Helpers/StringHelper";
import Numeral from "modulespackage/Grammar/Numeral";
import Guid from "modulespackage/Helpers/Guid";
import IResponce from "modulespackage/Models/IResponce";

import SystemConstants from "modulespackage/Constants/SystemConstants";
import axios from "axios";
import * as Cookies from "js-cookie";

import * as QueryString from "query-string";
import { Switch } from "@material-ui/core";

import "./Programme.types";

class Programme extends React.Component<any, IProgrammeState> {
	constructor(props) {
		super(props);

		this.routingData = Cookies.getJSON(SystemConstants.apiCookieName);
		this.urlParameters = QueryString.parse(window.location.search.toString());

		this.state = {
			loading: true,
			savingInProgress: false,
			error: {
				is: false,
				message: null
			}
		} as IProgrammeState;

		const documentDefaults = (additionalInfo: IAdditionalInfo): IDocument => ({
			chapter_1: {
				plainHtml: "К <b>основным целям</b> освоения дисциплины «"+additionalInfo.disciplineName+"» следует отнести формирование у обучающихся теоретических знаний и навыков в области ... <br />Задачи изучения дисциплины:",
				targets: [],
				goals: []
			},

			chapter_2: {
				contentHeader: "Дисциплина «"+additionalInfo.disciplineName+"» относится к числу профессиональных учебных дисциплин основной образовательной программы",
				disciplinesAfter: [],
				contentMiddle: "Изучение данной дисциплины базируется на следующих дисциплинахОсновные положения дисциплины должны быть использованы в дальнейшем при изучении следующих дисциплин:",
				disciplinesBefore: [],
			},

			chapter_3: {
				competenceTable: []
			},

			chapter_4: {
				disciplineOverallInfo: "",
				disciplineSectionsContent: []
			},

			chapter_5: {
				content: "Методика преподавания дисциплины «Физическое моделирование дополненной и виртуальной реальности» и реализация компетентностного подхода в изложении и восприятии материала предусматривает использование следующих активных и интерактивных форм проведения групповых, индивидуальных, аудиторных занятий в сочетании с внеаудиторной работой с целью формирования и развития профессиональных навыков обучающихся:<ul><li>подготовка к выполнению лабораторных работ в лабораториях вуза;</li><li>обсуждение и защита рефератов по дисциплине;</li><li>использование	интерактивных	форм	текущего	контроля	в	форме аудиторного и внеаудиторного интернет-тестирования.</li></ul>Удельный вес занятий, проводимых в интерактивных формах, определен главной целью образовательной программы, особенностью контингента обучающихся и содержанием дисциплины «Физическое моделирование дополненной и виртуальной реальности» и в целом по дисциплине составляет ... аудиторных занятий. Занятия лекционного типа составляют ... от объема аудиторных занятий.",
			},

			chapter_6: {
				contentHeader: "В процессе обучения используются следующие оценочные формы самостоятельной работы студентов, оценочные средства текущего контроля успеваемости и промежуточных аттестаций:",
				termsPlan: [],
				contentFooter: "Образцы тестовых заданий, контрольных вопросов и заданий для проведения текущего контроля, экзаменационных билетов, приведены в приложении.",
			}
		});
		

		(async () => {
			var [programmeResponse, additionalInfoResponse, disciplinesResponse] = 
				await axios.all([
					axios.get<IResponce<any>>(this.routingData.getProgramme+ "?uid=" + (this.urlParameters.uid == 'null' ? "00000000-0000-0000-0000-000000000000" : this.urlParameters.uid)),
					axios.get<IResponce<any>>(this.routingData.getDisciplineAdditionalInfo + "?disciplineUid=" + this.urlParameters.disciplineUid),
					axios.get<IResponce<any>>(this.routingData.getDisciplines)
				]);
			var competencesResponse = 
				await axios.get<IResponce<Array<ICompetence>>>(this.routingData.getCompetences+ "?year=" + additionalInfoResponse.data.data.year);

			this.programme = programmeResponse.data.data;

			this.setState({
				loading: false,
				repertories: {
					competences: competencesResponse.data.data,
					disciplines: disciplinesResponse.data.data as Array<string>
				},
				additionalInformation: additionalInfoResponse.data.data as IAdditionalInfo,
				document: programmeResponse.data.data == null
					? documentDefaults(additionalInfoResponse.data.data)
					: JSON.parse(programmeResponse.data.data.content) as IDocument,
				chapter_1_plainHtmlSwitch: false
			});

			var [timeScheduleForDisciplineInTextFormatResponce, timeScheduleForDisciplineResponce] = 
				await axios.all([
					axios.get<IResponce<any>>(this.routingData.getTimeScheduleForDisciplineInTextFormat+ "?disciplineUid=" + this.urlParameters.disciplineUid),
					axios.get<IResponce<any>>(this.routingData.getTimeScheduleForDiscipline+ "?disciplineUid=" + this.urlParameters.disciplineUid),
				]);

			this.setState(s => ({
				...s, 
				timeSchedule: timeScheduleForDisciplineResponce.data.data as Array<ITimeSchedule>,
				document: { 
					...s.document, 
					chapter_4: { 
						...s.document.chapter_4, 
						disciplineOverallInfo: 
						timeScheduleForDisciplineInTextFormatResponce.data.data as string
					} 
				}
			}));

			var chapter_4 = this.state.document.chapter_4;
			var chapter_6 = this.state.document.chapter_6;
			this.state.timeSchedule.forEach(element => {
				if(chapter_4.disciplineSectionsContent.find(x => x.term == element.term) == null){
					chapter_4.disciplineSectionsContent.push({term: element.term, sectionContent: []})
				}
				if(chapter_6.termsPlan.find(x => x.term == element.term) == null){
					chapter_6.termsPlan.push({term: element.term, sectionContent: []})
				}
			});

			this.setState(s => ({
				...s, 
				document: { 
					...s.document, 
					chapter_4: chapter_4,
					chapter_6: chapter_6
				}
			}));
		})();
	}

	private routingData; 
	private urlParameters;
	private programme: IProgrammeContract;

	private onSave = () => {
		this.setState(state => ({
			...state, 
			stateUpdateSignal: Guid.get(), 
			expandedPanelUid: null,
			savingInProgress: true,
			error: { is:false, message: null }
		}));

		this.programme = this.programme || {} as IProgrammeContract;
		this.programme.content = JSON.stringify(this.state.document);

		var request = {
			programme: this.programme,
			disciplineUid: this.urlParameters.disciplineUid
		};

		var error = (message: string) => this.setState(s => ({...s, error: { is: true, message: message}}));

		axios
			.post<IResponce<IProgrammeContract>>(this.routingData.saveProgramme, request)
			.then(responce => {
				console.log(responce);
				if(responce.data.isError){
					error(responce.data.message)
					return;
				}

				this.programme = responce.data.data
				window.history.pushState({},"", window.location.toString().replace("null", responce.data.data.uid));
 
			})
			.catch(reason => error(reason.response.statusText))
			.finally(()=> this.setState(s => ({...s, savingInProgress: false})))
	}

	private onExpansionPanelChange = (expandedUid: string) => {
		this.setState(state => ({ ...state, expandedPanelUid: expandedUid }));
	};

	private richEditorButtonsConfigurationButtons = [ 'undo', 'redo', '|', 'bold', 'strikethrough', 'underline', 'italic', '|', 'superscript', 'subscript', '|', 'ul', 'ol', '|', '\n', 'selectall', 'cut', 'copy', 'paste', '|', 'fullsize', 'print' ];
	private richEditorButtonsConfiguration = {
		readonly:false,
		buttons: this.richEditorButtonsConfigurationButtons,
		buttonsMD: this.richEditorButtonsConfigurationButtons,
		buttonsSM: this.richEditorButtonsConfigurationButtons,
		buttonsXS: this.richEditorButtonsConfigurationButtons,
	};

	render() {
		const doc = this.state.document;
		const additionalInfo = this.state.additionalInformation;
		const reps = this.state.repertories;
		const ePUid = this.state.expandedPanelUid;

		return this.state.loading 
		? (
			<Box display="flex" height={1} justifyContent="center" alignItems="center">
				<CircularProgress size={40} />
			</Box>
		)
		: (
			<div>
				{/* 
				<pre>
					{JSON.stringify(this.state, null, 2)}
				</pre>
				*/}
				<Typography paragraph variant="h5" align="center">
					Рабочая программа дисциплины
				</Typography>
				<Box>
					<Typography variant="subtitle1">
						Дисциплина: <b>{additionalInfo.disciplineName}</b>
					</Typography>
					<Typography variant="subtitle1">
						Направление подготовки: <b>{additionalInfo.specialityCode} {additionalInfo.specialityName}</b>
					</Typography>
					<Typography variant="subtitle1">
						Профиль: <b>{additionalInfo.profileName}</b>
					</Typography>
					<Typography variant="subtitle1">
						Степень выпускника: <b>{additionalInfo.qualificationName}</b>
					</Typography>
					<Typography variant="subtitle1">
						Форма обучения: <b>{additionalInfo.trainingFormName}</b>
					</Typography>
					<Typography variant="subtitle1">
						Год: <b>{additionalInfo.year}</b>
					</Typography>
				</Box>
				<Box>
					<SimpleExpansionPanel header="1. Цели освоения дисциплины" expanded={ePUid} onChange={this.onExpansionPanelChange}>
						<StateBlock state={doc.chapter_1 as any} signal={this.state.stateUpdateSignal} onStateUpdate={newState => this.setState(state => ({...state, document: {...state.document, chapter_1: newState }}))}>
							{(state: any, update: Function) => (
								<React.Fragment>
								   <Switch
										checked={this.state.chapter_1_plainHtmlSwitch}
										onChange={e =>{ e.persist(); this.setState(s => ({ chapter_1_plainHtmlSwitch: e.target.checked }))}}
										color="primary"/> 
									<Typography component="span">Свободное редактирование</Typography>
									{this.state.chapter_1_plainHtmlSwitch
									?(
										<JoditEditor
											ref={null}
											value={state.plainHtml}
											config={this.richEditorButtonsConfiguration}
											onBlur={newContent =>{ state.plainHtml = newContent; update()}}/>
									)
									:(
										<React.Fragment>
											<Typography>Цели</Typography>
											<ComplexList collection={state.targets} isBasicTyped empty={""} onCollectionChanged={(newCollection) => { state.targets = newCollection; update()}}>
												{(item: any, itemUpdate: Function, entryListUpdate: Function) => (
													<Input placeholder="Цель" fullWidth value={item.value} onChange={event => { item.value = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
												)}
											</ComplexList>

											<Typography>Задачи</Typography>
											<ComplexList collection={state.goals} isBasicTyped empty={""} onCollectionChanged={(newCollection) => { state.goals = newCollection; update() }}>
												{(item: any, itemUpdate: Function, entryListUpdate: Function) => (
													<Input placeholder="Задача" fullWidth value={item.value} onChange={event => { item.value = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
												)}
											</ComplexList>
										</React.Fragment>
									)}
								</React.Fragment>
							)}
						</StateBlock>
					</SimpleExpansionPanel>
					<SimpleExpansionPanel header="2. Место дисциплины в структуре ООП бакалавриата" expanded={ePUid} onChange={this.onExpansionPanelChange}>
						<StateBlock state={doc.chapter_2 as any} signal={this.state.stateUpdateSignal} onStateUpdate={newState => this.setState(state => ({...state, document: {...state.document, chapter_2: newState }}))}>
							{(state: any, update: Function) => (
								<div>
									<Typography variant="subtitle1">Описание</Typography>
									<Input 
										multiline
										fullWidth 
										value={state.contentHeader} 
										onChange={event => {event.persist(); state.contentHeader = event.target.value; update()}}/>
									<br /><br />
									<Typography variant="subtitle1">Изучение данной дисциплины базируется на следующих дисциплинах:</Typography>
									<ComplexList collection={state.disciplinesBefore} isBasicTyped empty={""} onCollectionChanged={(newCollection) => { state.disciplinesBefore = newCollection; update() }}>
										{(item: any, itemUpdate: Function, entryListUpdate: Function) => (
											<SimpleSelect
												value={item.value}
												list={reps.disciplines.map(x => ({ value: x, text: x }))}
												onChange={(newCode) => { item.value = newCode; entryListUpdate() }} />
										)}
									</ComplexList>
									<Typography variant="subtitle1">Описание</Typography>
									<Input 
										multiline
										fullWidth 
										value={state.contentMiddle} 
										onChange={event => { event.persist(); state.contentMiddle = event.target.value; update()}}/>
									<br /><br />
									<Typography variant="subtitle1">Дисциплины, доступные для изучения после этой:</Typography>
									<ComplexList collection={state.disciplinesAfter} isBasicTyped empty={""} onCollectionChanged={(newCollection) => { state.disciplinesAfter = newCollection; update() }}>
										{(item: any, itemUpdate: Function, entryListUpdate: Function) => (
											<SimpleSelect
												value={item.value}
												list={reps.disciplines.map(x => ({ value: x, text: x }))}
												onChange={(newCode) => { item.value = newCode; entryListUpdate() }} />
										)}
									</ComplexList>
									<br /><br />
								</div>
							)}
						</StateBlock>
					</SimpleExpansionPanel>
					<SimpleExpansionPanel header="3. Перечень планируемых результатов обучения по дисциплине (модулю) соотнесённые с планируемыми результатами освоения образовательной программы" expanded={ePUid} onChange={this.onExpansionPanelChange}>
						<StateBlock state={doc.chapter_3 as any} signal={this.state.stateUpdateSignal} onStateUpdate={newState => this.setState(state => ({...state, document: {...state.document, chapter_3: newState }}))}>
							{(state: any, update: Function) => (
								<ComplexList
									collection={state.competenceTable}
									isBasicTyped={false}
									empty={{ competenceCode: "", trainingResult: { know: "", can: "", able: "" } }}
									onCollectionChanged={(newCollection) => { state.competenceTable = newCollection; update() }}>
									{(item: any, itemUpdate: Function, entryListUpdate: Function) => (
										<Box mb={3}>
											<Grid container spacing={1}>
												<Grid container item xs={12} sm={2}>
													<SimpleSelect
														value={item.competenceCode}
														list={reps.competences.map(x => ({ value: x.code, text: x.code }))}
														onChange={(newCode) => { item.competenceCode = newCode; entryListUpdate() }} />
												</Grid>
												<Grid container item xs={12} sm={5}>
													<Typography>
														{(() => {
															var competence = reps.competences.find(x => x.code === item.competenceCode)
															return competence == null 
																? ""
																: competence.description
														})()}
													</Typography>
												</Grid>
												<Grid container item xs={12} sm={5}>
													<Input placeholder="Знать" fullWidth value={item.trainingResult.know} onChange={event => { item.trainingResult.know = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
													<Input placeholder="Уметь" fullWidth value={item.trainingResult.can} onChange={event => { item.trainingResult.can = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
													<Input placeholder="Владеть" fullWidth value={item.trainingResult.able} onChange={event => { item.trainingResult.able = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
												</Grid>
											</Grid>
										</Box>
									)}
								</ComplexList>
							)}
						</StateBlock>
					</SimpleExpansionPanel>
					<SimpleExpansionPanel header="4. Структура и содержания дисциплины" expanded={ePUid} onChange={this.onExpansionPanelChange}>
						<StateBlock state={doc.chapter_4 as any} signal={this.state.stateUpdateSignal} onStateUpdate={newState => this.setState(state => ({...state, document: {...state.document, chapter_4: newState }}))}>
							{(state: any, update: Function) => (
								<Box>
									<Typography>{doc.chapter_4.disciplineOverallInfo}</Typography>
									<Typography paragraph variant="h6" align="center">
										Содержание разделов дисциплины
									</Typography>
									{state.disciplineSectionsContent.map((dsc) => (
										<Box key={dsc.term}>
											<Typography>
												<b>
													{StringHelper.firstLetterUppercase(Numeral.getOrdinal(dsc.term))} семестр
												</b>
											</Typography>
											<ComplexList 
												collection={dsc.sectionContent} 
												isBasicTyped={false}
												empty={{header:"", content: ""}} 
												onCollectionChanged={(newCollection) => { dsc.sectionContent = newCollection;  update(); }}>
													{(item: any, itemUpdate: Function, entryListUpdate: Function) => (
														<Box>
															<Input 
																placeholder="Заголовок" 
																style={{fontWeight: 'bold'}}
																fullWidth 
																value={item.header} 
																onChange={event => { item.header = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
															<Input 
																placeholder="Содержание"
																multiline
																fullWidth 
																value={item.content} 
																onChange={event => { item.content = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
														</Box>
													)}
											</ComplexList>
										</Box>
									))}
								</Box>
							)}
						</StateBlock>
					</SimpleExpansionPanel>
					<SimpleExpansionPanel header="5. Образовательные технологии" expanded={ePUid} onChange={this.onExpansionPanelChange}>
						<StateBlock state={doc.chapter_5 as any} signal={this.state.stateUpdateSignal} onStateUpdate={newState => this.setState(state => ({...state, document: {...state.document, chapter_5: newState }}))}>
							{(state: any, update: Function) => (
								<JoditEditor
									ref={null}
									value={state.content}
									config={this.richEditorButtonsConfiguration}
									onBlur={newContent =>{ state.content = newContent; update()}}
									onChange={newContent => {}}/>
							)}
						</StateBlock>
					</SimpleExpansionPanel>
					<SimpleExpansionPanel header="6. Оценочные средства для текущего контроля успеваемости, промежуточной аттестации по итогам освоения дисциплины и учебно-методическое обеспечение самостоятельной работы студентов" expanded={ePUid} onChange={this.onExpansionPanelChange}>
						<StateBlock state={doc.chapter_6 as any} signal={this.state.stateUpdateSignal} onStateUpdate={newState => this.setState(state => ({...state, document: {...state.document, chapter_6: newState }}))}>
							{(state: any, update: Function) => (
								<Box>
									<Box mb={1}>
										<JoditEditor
											ref={null}
											value={state.contentHeader}
											config={this.richEditorButtonsConfiguration}
											onBlur={newContent =>{ state.contentHeader = newContent; update()}}
											onChange={newContent => {}}/>
									</Box>
									{state.termsPlan.map((tp) => (
										<Box key={tp.term}>
											<Typography>
												<b>
													{StringHelper.firstLetterUppercase(Numeral.getOrdinalPrepositional(tp.term, ["в", "во"]))} семестре
												</b>
											</Typography>
											<ComplexList 
												collection={tp.sectionContent} 
												isBasicTyped
												empty={""} 
												onCollectionChanged={(newCollection) => {tp.sectionContent = newCollection; update() }}>
													{(item: any, itemUpdate: Function, entryListUpdate: Function) => (
														<Input 
															fullWidth 
															value={item.value} 
															onChange={event => { item.value = event.target.value; itemUpdate() }} onBlur={() => entryListUpdate()} />
													)}
											</ComplexList>
										</Box>
									))}
									<Box mt={1}>
										<JoditEditor
											ref={null}
											value={state.contentFooter}
											config={this.richEditorButtonsConfiguration}
											onBlur={newContent => { state.contentFooter = newContent; update() }}
											onChange={newContent => {}}/>
									</Box>
								</Box>
							)}
						</StateBlock>
					</SimpleExpansionPanel>
				</Box>

				<Box mt={1}>
					{this.state.error.is 
						? <Alert severity="error">{this.state.error.message}</Alert>
						: null}

					{this.state.savingInProgress
						? <Container style={{ textAlign: "center" }}><CircularProgress size={34} /></Container>	
						: <Button onClick={this.onSave}>Сохранить</Button>}		
				</Box>							
			</div>
		);
	}
}



ReactDOM.render(
	<ThemeProvider theme={createMuiTheme(theme)}>
		<Programme/>
	</ThemeProvider>,
	document.getElementById("root")
);