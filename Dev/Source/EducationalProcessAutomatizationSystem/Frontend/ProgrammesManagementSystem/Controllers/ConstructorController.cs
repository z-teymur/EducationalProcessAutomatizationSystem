﻿using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using Microsoft.AspNetCore.Mvc;
using ProgrammesManagementSystem.Resources;

namespace ProgrammesManagementSystem.Controllers
{
	public class ConstructorController : Controller
	{
		[HttpGet]
		[Menu(nameof(MenuResources.ProgrammesPage))]
		[ApiRoutes(ApiConfig.ProgrammesConstructorApi)]
		public IActionResult Programme() => View();

		[HttpGet]
		public IActionResult ValuationFund() => View();
	}
}
