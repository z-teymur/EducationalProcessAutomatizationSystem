﻿using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using Microsoft.AspNetCore.Mvc;
using ProgrammesManagementSystem.Resources;

namespace ProgrammesManagementSystem.Controllers
{
	public class HomeController : Controller
	{
		[HttpGet]
		[Menu(nameof(MenuResources.HomePage))]
		public IActionResult Index() => View();

		[HttpGet]
		[Menu(nameof(MenuResources.ProgrammesPage))]
		[ApiRoutes(ApiConfig.ProgrammesApi)]
		public IActionResult Programmes() => View();
	}
}
