﻿using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Contract;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Interactor;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Token;
using EducationalProcessAutomatizationSystem.Common.CommonModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProgrammesManagementSystem.Resources;

namespace ProgrammesManagementSystem.Controllers
{
	[ApiController]
	[DefaultRoute]
	public class HomeApiController : ControllerBase
	{
		public HomeApiController(
			IProgrammesManagementSystemServiceInteractor programmesManagementSystemServiceInteractor,
			ILogger<HomeController> logger)
		{
			_logger = logger;
			_programmesManagementSystemServiceInteractor = programmesManagementSystemServiceInteractor;
		}

		[HttpGet]
		[Rights(nameof(MenuResources.ProgrammesPage))]
		public Response<List<DisciplineListItem>> GetProgrammes()
		{
			_logger.BeginScope(nameof(GetProgrammes));
			_programmesManagementSystemServiceInteractor.UseBearerJwt(TokenHttpContextProvider.GetTokenFromCookie(HttpContext));

			_logger.LogInformation("Getting programmes for current user");
			return _programmesManagementSystemServiceInteractor.GetDisciplinesForCurrentUser();
		}

		private readonly ILogger<HomeController> _logger;
		private readonly IProgrammesManagementSystemServiceInteractor _programmesManagementSystemServiceInteractor;
	}
}
