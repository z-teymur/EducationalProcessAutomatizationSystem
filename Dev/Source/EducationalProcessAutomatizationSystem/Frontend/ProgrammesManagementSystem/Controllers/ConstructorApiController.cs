﻿using System;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Contract;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Interactor;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Token;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ProgrammesManagementSystem.Controllers
{
	[ApiController]
	[DefaultRoute]
	public class ConstructorApiController : ControllerBase
	{
		public ConstructorApiController(ILogger<ConstructorApiController> logger, IProgrammesManagementSystemServiceInteractor programmesManagementSystemServiceInteractor)
		{
			_logger = logger;
			_programmesManagementSystemServiceInteractor = programmesManagementSystemServiceInteractor;
		}

		[HttpGet]
		public IActionResult GetProgramme(Guid uid)
		{
			using var s = _logger.BeginScope(nameof(GetProgramme));
			_programmesManagementSystemServiceInteractor.UseBearerJwt(TokenHttpContextProvider.GetTokenFromCookie(HttpContext));

			_logger.LogInformation($"Getting programme {uid}");
			var programme = _programmesManagementSystemServiceInteractor.GetProgramme(uid);

			_logger.LogInformation("Returning result");
			return new JsonResult(programme);
		}

		[HttpGet]
		public IActionResult GetTimeScheduleForDisciplineInTextFormat(Guid disciplineUid)
		{
			using var s = _logger.BeginScope(nameof(GetTimeScheduleForDisciplineInTextFormat));
			_programmesManagementSystemServiceInteractor.UseBearerJwt(TokenHttpContextProvider.GetTokenFromCookie(HttpContext));

			_logger.LogInformation($"Getting time schedule in text format {disciplineUid}");
			var timeScheduleForDisciplineInTextFormat = _programmesManagementSystemServiceInteractor.GetTimeScheduleForDisciplineInTextFormat(disciplineUid);

			_logger.LogInformation("Returning result");
			return new JsonResult(timeScheduleForDisciplineInTextFormat);
		}

		[HttpGet]
		public IActionResult GetTimeScheduleForDiscipline(Guid disciplineUid)
		{
			using var s = _logger.BeginScope(nameof(GetTimeScheduleForDiscipline));
			_programmesManagementSystemServiceInteractor.UseBearerJwt(TokenHttpContextProvider.GetTokenFromCookie(HttpContext));

			_logger.LogInformation($"Getting time schedule {disciplineUid}");
			var timeSchedule = _programmesManagementSystemServiceInteractor.GetTimeScheduleForDiscipline(disciplineUid);

			_logger.LogInformation("Returning result");
			return new JsonResult(timeSchedule);
		}

		[HttpPost]
		public IActionResult SaveProgramme(ProgrammeWithDisciplineUid programmeWithDisciplineUid)
		{
			using var s = _logger.BeginScope(nameof(SaveProgramme));
			_programmesManagementSystemServiceInteractor.UseBearerJwt(TokenHttpContextProvider.GetTokenFromCookie(HttpContext));

			_logger.LogInformation($"Saving the programme {programmeWithDisciplineUid.Programme.Uid}");
			var result = _programmesManagementSystemServiceInteractor.SaveProgramme(programmeWithDisciplineUid);

			_logger.LogInformation("Returning result");
			return new JsonResult(result);
		}

		private readonly ILogger<ConstructorApiController> _logger;
		private readonly IProgrammesManagementSystemServiceInteractor _programmesManagementSystemServiceInteractor;
	}
}
