﻿using System;
using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Interactor;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Attributes;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Token;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ProgrammesManagementSystem.Controllers
{
	[ApiController]
	[DefaultRoute]
	public class AdditionalInformationApiController : ControllerBase
	{
		private readonly ILogger<AdditionalInformationApiController> _logger;
		private readonly IProgrammesManagementSystemServiceInteractor _programmesManagementSystemServiceInteractor;

		public AdditionalInformationApiController(
			IProgrammesManagementSystemServiceInteractor programmesManagementSystemServiceInteractor,
			ILogger<AdditionalInformationApiController> logger)
		{
			_logger = logger;
			_programmesManagementSystemServiceInteractor = programmesManagementSystemServiceInteractor;
		}


		[HttpGet]
		public IActionResult GetCompetences(int year)
		{
			using var s = _logger.BeginScope(nameof(GetCompetences));
			_programmesManagementSystemServiceInteractor.UseBearerJwt(TokenHttpContextProvider.GetTokenFromCookie(HttpContext));

			_logger.LogInformation($"Getting competences of {year} year");
			var competences = _programmesManagementSystemServiceInteractor.GetCompetences(year);

			_logger.LogInformation("Returning result");
			return new JsonResult(competences);
		}


		[HttpGet]
		public IActionResult GetDisciplines()
		{
			using var s = _logger.BeginScope(nameof(GetCompetences));
			_programmesManagementSystemServiceInteractor.UseBearerJwt(TokenHttpContextProvider.GetTokenFromCookie(HttpContext));

			_logger.LogInformation("Getting all disciplines");
			var disciplines = _programmesManagementSystemServiceInteractor.GetDisciplines();

			_logger.LogInformation("Returning result");
			return new JsonResult(disciplines);
		}


		[HttpGet]
		public IActionResult GetDisciplineAdditionalInfo(Guid disciplineUid)
		{
			using var s = _logger.BeginScope(nameof(GetCompetences));
			_programmesManagementSystemServiceInteractor.UseBearerJwt(TokenHttpContextProvider.GetTokenFromCookie(HttpContext));

			_logger.LogInformation($"Getting discipline additional info {disciplineUid}");
			var disciplines = _programmesManagementSystemServiceInteractor
				.GetDisciplineAdditionalInfo(disciplineUid);

			_logger.LogInformation("Returning result");
			return new JsonResult(disciplines);
		}
	}
}
