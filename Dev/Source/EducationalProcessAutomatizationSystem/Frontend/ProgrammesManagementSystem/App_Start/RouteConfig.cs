﻿using EducationalProcessAutomatizationSystem.Common.Extensions.Routing;
using Microsoft.AspNetCore.Routing;
using ProgrammesManagementSystem.Controllers;

namespace ProgrammesManagementSystem
{
	public static class RouteConfig
	{
		public static void Configure(IEndpointRouteBuilder routes)
		{
			routes.MapControllerAbsoluteRoute(
				"/",
				nameof(HomeController),
				nameof(HomeController.Index));

			routes.MapControllerAbsoluteRoute(
				"/Programmes",
				nameof(HomeController),
				nameof(HomeController.Programmes));

			routes.MapControllerAbsoluteRoute(
				"/Constructor/Programme",
				nameof(ConstructorController),
				nameof(ConstructorController.Programme));

			routes.MapControllerAbsoluteRoute(
				"/Constructor/ValuationFund",
				nameof(ConstructorController),
				nameof(ConstructorController.ValuationFund));

			routes.MapControllerAbsoluteRoute(
				"/Programme/Download",
				nameof(ProgrammeController),
				nameof(ProgrammeController.Download));
		}
	}
}
