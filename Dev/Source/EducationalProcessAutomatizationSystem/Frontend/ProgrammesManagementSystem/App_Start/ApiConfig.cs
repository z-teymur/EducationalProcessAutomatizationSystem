﻿using System;
using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Api;
using EducationalProcessAutomatizationSystem.Common.AspUtils.Models;
using Microsoft.Extensions.DependencyInjection;
using ProgrammesManagementSystem.Controllers;

namespace ProgrammesManagementSystem
{
	public static class ApiConfig
	{
		public const string ProgrammesApi = nameof(ProgrammesApi);
		public const string ProgrammesConstructorApi = nameof(ProgrammesConstructorApi);

		public static void ConfigureApiRoutes(this IServiceCollection services)
		{
			var sectionDictionary = new Dictionary<string, Dictionary<string, Uri>>
			{
				{
					ProgrammesApi,
					new Dictionary<string, Uri>
					{
						{
							nameof(HomeApiController.GetProgrammes),
							ApiUriCreator.Create(nameof(HomeApiController), nameof(HomeApiController.GetProgrammes))
						},
						{
							nameof(ProgrammeController.Download),
							ApiUriCreator.Create(nameof(ProgrammeController), nameof(ProgrammeController.Download))
						},
						{
							"Edit",
							new Uri("/Constructor/Programme", UriKind.Relative)
						}
					}
				},
				{
					ProgrammesConstructorApi,
					new Dictionary<string, Uri>
					{
						{
							nameof(ConstructorApiController.GetProgramme),
							ApiUriCreator.Create(nameof(ConstructorApiController), nameof(ConstructorApiController.GetProgramme))
						},
						{
							nameof(ConstructorApiController.GetTimeScheduleForDisciplineInTextFormat),
							ApiUriCreator.Create(nameof(ConstructorApiController), nameof(ConstructorApiController.GetTimeScheduleForDisciplineInTextFormat))
						},
						{
							nameof(ConstructorApiController.GetTimeScheduleForDiscipline),
							ApiUriCreator.Create(nameof(ConstructorApiController), nameof(ConstructorApiController.GetTimeScheduleForDiscipline))
						},
						{
							nameof(ConstructorApiController.SaveProgramme),
							ApiUriCreator.Create(nameof(ConstructorApiController), nameof(ConstructorApiController.SaveProgramme))
						},
						{
							nameof(AdditionalInformationApiController.GetCompetences),
							ApiUriCreator.Create(nameof(AdditionalInformationApiController), nameof(AdditionalInformationApiController.GetCompetences))
						},
						{
							nameof(AdditionalInformationApiController.GetDisciplines),
							ApiUriCreator.Create(nameof(AdditionalInformationApiController), nameof(AdditionalInformationApiController.GetDisciplines))
						},
						{
							nameof(AdditionalInformationApiController.GetDisciplineAdditionalInfo),
							ApiUriCreator.Create(nameof(AdditionalInformationApiController), nameof(AdditionalInformationApiController.GetDisciplineAdditionalInfo))
						}
					}
				}
			};

			services.AddSingleton(new ApiConfiguration(sectionDictionary));
		}
	}
}
