﻿using EducationalProcessAutomatizationSystem.Backend.ProgrammesManagementSystemService.Interactor;
using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using Microsoft.Extensions.DependencyInjection;

namespace ProgrammesManagementSystem
{
	public static class Bootstrapper
	{
		public static void RegisterDependencies(this IServiceCollection services)
		{
			services.AddScoped(provider => MenuConfig.Configure());
			services.AddProgrammesManagementSystemServiceInteractor(SystemRoutingConfiguration.ProgrammesManagementSystemServiceApiUri);
		}
	}
}
