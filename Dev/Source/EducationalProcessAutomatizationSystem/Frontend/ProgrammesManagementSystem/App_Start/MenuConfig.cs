﻿using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Constants;
using EducationalProcessAutomatizationSystem.Frontend.Common.Menu;
using ProgrammesManagementSystem.Resources;

namespace ProgrammesManagementSystem
{
	public static class MenuConfig
	{
		public static IFrontendMenu Configure()
		{
			var globalMenu = new GlobalMenuFactory().GetGlobalMenu();

			var menu = new List<MenuItem>
			{
				new MenuItem(
					"/Menu/menu-home.svg",
					MenuResources.HomePage,
					nameof(MenuResources.HomePage),
					"/",
					Roles.Everybody),
				new MenuItem(
					"/Menu/menu-programmes.svg",
					MenuResources.ProgrammesPage,
					nameof(MenuResources.ProgrammesPage),
					"/programmes",
					Roles.Teacher)
			};

			return new FrontendMenu(globalMenu, menu);
		}
	}
}
