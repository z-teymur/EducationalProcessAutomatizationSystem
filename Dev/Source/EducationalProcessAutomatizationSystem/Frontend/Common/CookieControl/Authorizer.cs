﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EducationalProcessAutomatizationSystem.Backend.MainService.Contract;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.CookieControl
{
	public class Authorizer : IAuthorizer
	{
		public Authorizer(
			IClaimConverter claimConverter)
		{
			_claimConverter = claimConverter;
		}

		public const string UserAgentHeader = "User-Agent";

		public async Task CreateCookieAsync(HttpContext context, UserCredentials credentials)
		{
			var claims = _claimConverter.ConvertToClaims(credentials).ToList();

			claims.Add(new Claim(UserAgentHeader, context.Request.Headers[UserAgentHeader]));

			var identity = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

			await context.SignInAsync(
					CookieAuthenticationDefaults.AuthenticationScheme,
					new ClaimsPrincipal(identity),
					new AuthenticationProperties { IsPersistent = true })
				.ConfigureAwait(false);
		}

		public async Task SignOutAsync(HttpContext context)
		{
			await context
				.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme)
				.ConfigureAwait(false);
		}

		private readonly IClaimConverter _claimConverter;
	}
}
