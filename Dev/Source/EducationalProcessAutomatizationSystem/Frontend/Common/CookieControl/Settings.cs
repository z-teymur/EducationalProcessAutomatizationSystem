﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using EducationalProcessAutomatizationSystem.Backend.MainService.Interactor;
using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using EducationalProcessAutomatizationSystem.Common.Security.Cryptography;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.DependencyInjection;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.CookieControl
{
	public static class Settings
	{
		public static void AddCookieAuthorizationSettings(this IServiceCollection services)
		{
			services.AddMainServiceInteractor(SystemRoutingConfiguration.MainServiceApiUri);
			services.AddScoped<IHashProvider, HashProvider>();
			services.AddScoped<IClaimConverter, ClaimConverter>();
			services.AddScoped<IAuthorizer, Authorizer>();
			services.AddScoped<AuthenticationEvents>();

			services
				.AddDataProtection()
				.PersistKeysToFileSystem(new DirectoryInfo(AuthorizationCookieConfiguration.DataProtectionKeysLocation))
				.SetApplicationName("EducationalProcessAutomatizationSystem");
			services
				.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
				.AddCookie(options =>
				{
					options.Events.OnRedirectToAccessDenied = context =>
					{
						context.Response.StatusCode = (int)HttpStatusCode.NotFound;
						return Task.CompletedTask;
					};

					options.Cookie.Name = AuthorizationCookieConfiguration.CookieName;
					options.Cookie.Domain = $".{SystemRoutingConfiguration.Domain}";
					options.EventsType = typeof(AuthenticationEvents);
				});

			services.AddMvc(setup =>
			{
				var policy = new AuthorizationPolicyBuilder()
					.RequireAuthenticatedUser()
					.Build();
				setup.Filters.Add(new AuthorizeFilter(policy));
			});
		}
	}
}
