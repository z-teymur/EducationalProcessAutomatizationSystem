﻿using System.Collections.Generic;
using System.Security.Claims;
using EducationalProcessAutomatizationSystem.Backend.MainService.Contract;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.CookieControl
{
	public interface IClaimConverter
	{
		UserCredentials ConvertToUserCredentials(IEnumerable<Claim> claims, out string userAgent);

		IEnumerable<Claim> ConvertToClaims(UserCredentials credentials);
	}
}