﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using EducationalProcessAutomatizationSystem.Backend.MainService.Contract;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.CookieControl
{
	internal class ClaimConverter : IClaimConverter
	{
		public UserCredentials ConvertToUserCredentials(IEnumerable<Claim> claims, out string userAgent)
		{
			userAgent = null;
			var userCredentials = new UserCredentials
			{
				Roles = new List<string>()
			};

			foreach (var claim in claims)
			{
				switch (claim.Type)
				{
					case nameof(UserCredentials.Token):
						userCredentials.Token = claim.Value;
						break;
					case nameof(UserCredentials.Password):
						userCredentials.Password = claim.Value;
						break;
					case ClaimsIdentity.DefaultNameClaimType:
						userCredentials.Uid = Guid.Parse(claim.Value);
						break;
					case ClaimsIdentity.DefaultRoleClaimType:
						userCredentials.Roles.Add(claim.Value);
						break;
					case Authorizer.UserAgentHeader:
						userAgent = claim.Value;
						break;
					default:
						break;
				}
			}

			return userCredentials;
		}

		public IEnumerable<Claim> ConvertToClaims(UserCredentials credentials)
		{
			var claims = new List<Claim>
			{
				new Claim(ClaimsIdentity.DefaultNameClaimType, credentials.Uid.ToString()),
				new Claim(nameof(UserCredentials.Password), credentials.Password),
				new Claim(nameof(UserCredentials.Token), credentials.Token)
			};

			claims.AddRange(credentials.Roles.Select(role => new Claim(ClaimsIdentity.DefaultRoleClaimType, role)));

			return claims;
		}
	}
}
