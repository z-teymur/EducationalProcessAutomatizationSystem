﻿using System.Net.Http;
using System.Threading.Tasks;
using EducationalProcessAutomatizationSystem.Backend.MainService.Interactor;
using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.CookieControl
{
	public class AuthenticationEvents : CookieAuthenticationEvents
	{
		public AuthenticationEvents(
			IAuthorizer authorizer,
			IClaimConverter claimConverter,
			IMainServiceInteractor mainServiceInteractor)
		{
			_authorizer = authorizer;
			_claimConverter = claimConverter;
			_mainServiceInteractor = mainServiceInteractor;
		}

		public override Task RedirectToLogin(RedirectContext<CookieAuthenticationOptions> context)
		{
			context.HttpContext.Response.Redirect($"{SystemRoutingConfiguration.Protocol}://{SystemRoutingConfiguration.Domain}/Login");
			return Task.CompletedTask;
		}

		public override Task RedirectToAccessDenied(RedirectContext<CookieAuthenticationOptions> context)
		{
			return RedirectToLogin(context);
		}

		public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
		{
			var userCredentials = _claimConverter.ConvertToUserCredentials(context.Principal.Claims, out var userAgent);

			try
			{
				if (context.Request.Headers[Authorizer.UserAgentHeader].Equals(userAgent))
				{
					var response = _mainServiceInteractor.Validate(userCredentials);
					if (!response.IsError)
					{
						await _authorizer.CreateCookieAsync(context.HttpContext, response.Data).ConfigureAwait(false);
						return;
					}
				}
			}
			catch (HttpRequestException)
			{
			}

			context.RejectPrincipal();
			await _authorizer
				.SignOutAsync(context.HttpContext)
				.ConfigureAwait(false);
		}

		private readonly IAuthorizer _authorizer;
		private readonly IMainServiceInteractor _mainServiceInteractor;
		private readonly IClaimConverter _claimConverter;
	}
}
