﻿using System.Threading.Tasks;
using EducationalProcessAutomatizationSystem.Backend.MainService.Contract;
using Microsoft.AspNetCore.Http;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.CookieControl
{
	public interface IAuthorizer
	{
		Task CreateCookieAsync(HttpContext context, UserCredentials credentials);

		Task SignOutAsync(HttpContext context);
	}
}