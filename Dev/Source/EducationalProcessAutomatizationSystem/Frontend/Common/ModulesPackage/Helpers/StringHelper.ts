export default class StringHelper {
	public static firstLetterUppercase = (value: string): string => {
		return value.charAt(0).toUpperCase() + value.slice(1);
	}
}