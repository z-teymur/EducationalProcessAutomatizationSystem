import * as Cookies from "js-cookie";

export default class CookieStorage {
	public static get = (name: string, def: string): string => {
		let val = Cookies.get(name);
		if (val === undefined) {
			val = def;
			Cookies.set(name, val);
		}

		return val;
	}

	public static store = (name: string, val: string)  => {
		Cookies.set(name, val);
	}


	public static getBoolean = (name: string, def: boolean): boolean => CookieStorage.get(name, def.toString()) === "true";
	public static storeBoolean = (name: string, value: boolean) => CookieStorage.store(name, value.toString());


}
