export class TextFieldValidation {
	constructor(errors: object, touched: object) {
		this.errors = errors;
		this.touched = touched;

	}

	private readonly errors: object;
	private readonly touched: object;

	public validate = (name: string): object => {
		return {
			error: this.errors[name] && this.touched[name],
			helperText: (this.errors[name] && this.touched[name]) && this.errors[name]
		}
	}
}