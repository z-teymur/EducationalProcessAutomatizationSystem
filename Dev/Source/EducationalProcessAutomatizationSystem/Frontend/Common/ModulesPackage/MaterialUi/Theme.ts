export default {
	palette: {
		primary: { main: "#000000" }
	},
	props: {
		MuiTextField: {
			fullWidth: true,
			variant: 'outlined' as "outlined",
			margin: "normal" as "normal",
			size: "small" as "small"
		},
		MuiButton: {
			fullWidth: true,
			variant:"contained" as "contained",
			color: "primary" as "primary"
		},
	}
}