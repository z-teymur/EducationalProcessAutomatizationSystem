export default interface IResponce<TData> {
    data: TData;
    message: string;
    isError: boolean;
}