export default class Numeral {
	public static getOrdinal = (numeral: number) => {
		switch (numeral) {
			case 0: return "нулевой";
			case 1: return "первый";
			case 2: return "второй";
			case 3: return "третьий";
			case 4: return "четвёртый";
			case 5: return "пятый";
			case 6: return "шестой";
			case 7: return "седьмой";
			case 8: return "восьмой";
			case 9: return "девятый";
			case 10: return "десятый";
			case 11: return "одинадцатый";
			case 12: return "двенадцатый";
			case 13: return "тринадцатый";
			default:
				throw "Unknown ordinal numeral exception";
		}
	}

	public static getOrdinalPrepositional = (numeral: number, preposition: Array<string>) => {
		switch (numeral) {
			case 0: return preposition[0] + " " + "нулевом";
			case 1: return preposition[0] + " " + "первом";
			case 2: return preposition[1] + " " + "втором";
			case 3: return preposition[0] + " " + "третьем";
			case 4: return preposition[0] + " " + "четвёртом";
			case 5: return preposition[0] + " " + "пятом";
			case 6: return preposition[0] + " " + "шестом";
			case 7: return preposition[0] + " " + "седьмом";
			case 8: return preposition[0] + " " + "восьмом";
			case 9: return preposition[0] + " " + "демятом";
			case 10: return preposition[0] + " " + "десятом";
			case 11: return preposition[0] + " " + "одинадцатом";
			case 12: return preposition[0] + " " + "двенадцатом";
			case 13: return preposition[0] + " " + "тринадцатом";
			default:
				throw "Unknown зrepositional numeral exception";
		}
	}
}