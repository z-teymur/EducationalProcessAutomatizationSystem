import * as React from "react";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";


interface ISimpleSelect {
	value: string | number;
	text:string;
}

interface ISimpleSelectProps {
	value: string | number;
	onChange(newValue: string | number): void;
	list: Array<ISimpleSelect>;
}


export default class SimpleSelect extends React.Component<ISimpleSelectProps, any> {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Select fullWidth value={this.props.value} onChange={(event) => { this.props.onChange(event.target.value as string | number)}} displayEmpty>
				{this.props.list.map(listItem => (
					<MenuItem value={listItem.value} key={listItem.value}>{listItem.text}</MenuItem>
				))}
			</Select>
		);
	}
}