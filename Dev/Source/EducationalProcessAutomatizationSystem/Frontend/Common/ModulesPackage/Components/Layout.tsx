
import * as React from "react";

import clsx from "clsx";

import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import Link from "@material-ui/core/Link";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { Theme, withStyles, StyleRules } from "@material-ui/core/styles";

import * as Cookies from "js-cookie";
import CookieStorage from "../Helpers/CookieStorage";

const drawerWidth = 240;
const drawerMinimizedCookieName = "MenuMinimized";
const menuCookieName = "Menu";

const styles = (theme: Theme) => ({
	root: { display: "flex" },
	drawer: {
		[theme.breakpoints.up("sm")]: {
			width: drawerWidth,
			flexShrink: 0,
		},
	},
	appBar: {
		[theme.breakpoints.up("sm")]: {
			display: "none"
		},
	},
	menuButton: {
		marginRight: theme.spacing(2),
		[theme.breakpoints.up("sm")]: {
			display: "none",
		}
	},
	menuIcon: {
		minWidth: '2em',
		width: '2em',
		minHeight: '2em',
		height: '2em',
		marginRight: '1em',
		"& img": {
			width: "100%",
			height: "100%"
		}
	},
	drawerPaper: {
		display: "flex",
		flexDirection: "column",
		width: drawerWidth,
		overflowX: "hidden"
	},
	drawerHeader: {
		flexGrow: 1,
		width: drawerWidth
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
		minHeight: "100vh"
	},
	drawerDesktopTransition: {
		[theme.breakpoints.up("sm")]: {
			transition: theme.transitions.create("width", {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen,
			}),
		}
	},
	drawerMinimize: {
		overflowX: "hidden",
		width: theme.spacing(7) + 1
	},
	desktopDrawerToggleButton: {

		alignSelf: "flex-end"
	}
} as StyleRules);

interface ILayoutState {
	mobileOpen: boolean;
	drawerMinimized: boolean;
}

class Layout extends React.Component<any, ILayoutState> {
	constructor(props) {
		super(props);

		this.state = {
			mobileOpen: false,
			drawerMinimized: CookieStorage.getBoolean(drawerMinimizedCookieName, false)
		};

		this.menuData = Cookies.getJSON(menuCookieName);
		this.classes = this.props.classes;
	}

	private classes;
	private menuData;

	private mobileDrawerToggle = () => { this.setState((state) => ({ ...state, mobileOpen: !state.mobileOpen }) as ILayoutState); }
	private desktopDrawerToggle = () => {
		CookieStorage.storeBoolean(drawerMinimizedCookieName, !this.state.drawerMinimized);
		this.setState((state) => ({ ...state, drawerMinimized: !state.drawerMinimized }) as ILayoutState);
	}

	private drawer = (menu: any) => (
		<List>
			{menu.map(menuItem => (
				<Link href={menuItem.route} rel="noopener" target="_self" key={menuItem.key}>
					<ListItem selected={menuItem.selected} button>
						<ListItemIcon className={this.classes.menuIcon}>
							<img src={menuItem.icon} />
						</ListItemIcon>
						<ListItemText primary={<Typography>{menuItem.name}</Typography>} />
					</ListItem>
				</Link>
			))}
	</List>
	);

	render = () => (
		<div className={this.classes.root}>
			<CssBaseline />
			<AppBar position="fixed" className={this.classes.appBar}>
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						edge="start"
						onClick={this.mobileDrawerToggle}
						className={this.classes.menuButton}>
						<MenuIcon />
					</IconButton>
					<Typography variant="h6" noWrap></Typography>
				</Toolbar>
			</AppBar>
			<nav className={clsx(this.classes.drawer, this.classes.drawerDesktopTransition, { [this.classes.drawerMinimize]: this.state.drawerMinimized })}>
				<Hidden smUp implementation="css">
					<Drawer
						variant="temporary"
						anchor={"left"}
						open={this.state.mobileOpen}
						onClose={this.mobileDrawerToggle}
						classes={{ paper: this.classes.drawerPaper }}
						ModalProps={{ keepMounted: true }}>
						<div className={this.classes.drawerHeader}>{this.drawer(this.menuData.menu)}</div>
						<Divider />
						<div>{this.drawer(this.menuData.globalMenu)}</div>
					</Drawer>
				</Hidden>
				<Hidden xsDown implementation="css">
					<Drawer
						classes={{ paper: clsx(this.classes.drawerPaper, this.classes.drawerDesktopTransition, { [this.classes.drawerMinimize]: this.state.drawerMinimized }) }}
						variant="permanent"
						open>

						<div className={this.classes.desktopDrawerToggleButton}>
							<IconButton onClick={this.desktopDrawerToggle}>
								{this.state.drawerMinimized
									? <ChevronRightIcon />
									: <ChevronLeftIcon />
								}
							</IconButton>
						</div>
						<Divider />
						<div className={this.classes.drawerHeader}>{this.drawer(this.menuData.menu)}</div>
						<Divider />
						<div>{this.drawer(this.menuData.globalMenu)}</div>
					</Drawer>
				</Hidden>
			</nav>
			<main className={this.classes.content}>
				{this.props.children}
			</main>
		</div>
	);
}

export default withStyles(styles)(Layout);