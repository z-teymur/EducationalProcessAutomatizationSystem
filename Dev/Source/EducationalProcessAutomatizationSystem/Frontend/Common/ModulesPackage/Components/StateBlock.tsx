import * as React from "react";



interface IStateBlockProps {
	state: any;
	signal?: any;
	onStateUpdate?(newState:any): any;
	children: (state: any, update: Function) => JSX.Element;
}


export default class StateBlock extends React.Component<IStateBlockProps, any> {
	constructor(props) {
		super(props);

		this.state = props.state;
		this.signal = props.signal;
	}

	private signal: any;

	componentDidUpdate() {
		if(this.props.signal != this.signal){
			this.signal = this.props.signal;
			if(this.props.onStateUpdate){
				this.props.onStateUpdate(this.state);
			}
		}
	}

	private updateState = () => {
		this.setState(this.state);
	}

	render() {
		return this.props.children(this.state, () => this.updateState());
	}
}

