import * as React from "react";

import Box from "@material-ui/core/Box";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";

import Guid from "../Helpers/Guid";

interface ISimpleExpansionPanelProps {
	header: string;
	expanded: string;
	onChange(uid: string);
}


export default class SimpleExpansionPanel extends React.Component<ISimpleExpansionPanelProps, any> {
	constructor(props) {
		super(props);
	}

	private uid: string = Guid.get();

	private expansionPanelChange = () => (event: React.ChangeEvent<{}>, isExpanded: boolean) => {
		this.props.onChange(isExpanded ? this.uid : "");
	};

	render() {
		return (
			<ExpansionPanel
				expanded={this.props.expanded === this.uid}
				onChange={this.expansionPanelChange()}>
				<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
					<Typography>{this.props.header}</Typography>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<Box width={1}>
						{this.props.children}
					</Box>
				</ExpansionPanelDetails>
			</ExpansionPanel>
		);
	}
}