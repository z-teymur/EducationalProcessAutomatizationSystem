import * as React from "react";

import { ReactSortable } from "react-sortablejs-typescript";

import Box from "@material-ui/core/Box";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import AddIcon from "@material-ui/icons/Add";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import UnfoldMore from "@material-ui/icons/UnfoldMore";

import Guid from "../Helpers/Guid";

interface IComplexListProps<TModel> {
	collection: Array<TModel>;
	empty: TModel;
	isBasicTyped: boolean;
	children: (item: TModel, itemUpdate: Function, entryListUpdate: Function) => JSX.Element;
	onCollectionChanged(newCollection: Array<TModel>): any;
}

interface IBasicModel {
	value: string
}

interface IComplexListItem<TModel> {
	key: string;
	item: TModel;
}

interface IComplexListState<TModel> {
	collection: Array<IComplexListItem<TModel>>;
}





export default class ComplexList<TModel> extends React.Component<IComplexListProps<TModel>, IComplexListState<TModel | IBasicModel>> {
	constructor(props) {
		super(props);

		this.state = {
			collection: props.collection.map(x => ({ item: this.getItem(x), key: Guid.get() } as IComplexListItem<TModel | IBasicModel>))
		}
	}

	private getItem = (item: TModel): TModel | IBasicModel => {
		return this.props.isBasicTyped
			? { value: item as unknown as string } as IBasicModel
			: item;
	} 

	private setItem = (item: TModel | IBasicModel): TModel => {

		if (this.props.isBasicTyped) {
			return (item as IBasicModel).value as unknown as TModel;
		} else {
			return item as TModel;
		}
	} 

	private update() {
		this.props.onCollectionChanged(
			this.state.collection.map(x => this.setItem(x.item)));
	}


	private handleSetList = (newList) => {
		this.setState(state => ({ collection: newList }));
		this.update();
	}

	private updateCollection = () => {
		this.setState(state => ({ collection: state.collection }));
	}

	private addToList = () => {
		this.setState(state => ({
			collection:
			[
				...state.collection,
				{ item: this.getItem(this.props.empty), key: Guid.get() }
			]
		} as IComplexListState<TModel | IBasicModel>));
		this.update();
	}

	private deleteFromList = (key: string) => {
		this.state.collection.splice(this.state.collection.map(x => x.key).indexOf(key), 1);
		this.setState(state => ({ ...state } as IComplexListState<TModel | IBasicModel>));
		this.update();
	}

	render() {
		return (
			<Box width={1}>
				{this.state.collection.length > 0 
				? (
					<List>
						<ReactSortable list={this.state.collection as any} handle=".sortable-handle" setList={this.handleSetList}>
							{this.state.collection.map(item => (
								<ListItem key={item.key}>
									<Box width={1} mr={10}>
										{this.props.children(item.item as TModel, () => this.updateCollection(), () => this.update())}
									</Box>
									<ListItemSecondaryAction className="sortable-handle">
										<IconButton>
											<UnfoldMore />
										</IconButton>
										<IconButton onClick={() => this.deleteFromList(item.key)}>
											<DeleteIcon />
										</IconButton>
									</ListItemSecondaryAction>
								</ListItem>
							))}
						</ReactSortable>
					</List>
				) 
				: null}

				<Box textAlign="left" mr={0.5}>
					<IconButton onClick={this.addToList}>
						<AddIcon/>
					</IconButton>
				</Box>
			</Box>
		);
	}
}

