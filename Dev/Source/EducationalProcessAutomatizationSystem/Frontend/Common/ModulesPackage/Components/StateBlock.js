"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var StateBlock = /** @class */ (function (_super) {
    __extends(StateBlock, _super);
    function StateBlock(props) {
        var _this = _super.call(this, props) || this;
        _this.updateState = function () {
            _this.setState(_this.state);
        };
        _this.state = props.state;
        _this.signal = props.signal;
        return _this;
    }
    StateBlock.prototype.componentDidUpdate = function () {
        if (this.props.signal != this.signal) {
            this.signal = this.props.signal;
            if (this.props.onStateUpdate) {
                this.props.onStateUpdate(this.state);
            }
        }
    };
    StateBlock.prototype.render = function () {
        var _this = this;
        return this.props.children(this.state, function () { return _this.updateState(); });
    };
    return StateBlock;
}(React.Component));
exports.default = StateBlock;
//# sourceMappingURL=StateBlock.js.map