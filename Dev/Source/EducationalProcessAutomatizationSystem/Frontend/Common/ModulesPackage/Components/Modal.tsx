import * as React from "react";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { createModal } from "react-modal-promise";


interface IModalButton {
	key: string;
	caption: string;
}

interface IModalProps {
	open: boolean;
	title: string;
	message: string;
	buttons?: Array<IModalButton>;
	onClose: Function;
}

class Modal extends React.Component<IModalProps, any> {
	constructor(props) {
		super(props);

		this.buttons = props.buttons;
	}
	protected buttons: Array<IModalButton>;


	render() {
		return (
			<Dialog
				open={this.props.open}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description">
				<DialogTitle id="alert-dialog-title">{this.props.title}</DialogTitle>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">{this.props.message}</DialogContentText>
				</DialogContent>
				<DialogActions>
					{this.buttons.map(button =>
						<Button key={button.key} onClick={() => { this.props.onClose(button.key) }} color="primary">{button.caption}</Button>
					)}
				</DialogActions>
			</Dialog>
		);
	}
}

class YesNoModal extends Modal {
	constructor(props) {
		super(props);
		this.buttons = [{ key: "yes", caption: "Да" }, { key: "no", caption: "Нет" }];
	}
}

class OkModal extends Modal {
	constructor(props) {
		super(props);
		this.buttons = [{ key: "ok", caption: "Ок" }];
	}
}

class ModalExecutor {
	public static executeYesNo = (title: string, message: string) => {
		const modal = ({ open, close }) =>
		(
			<YesNoModal open={open} title={title} message={message} onClose={close} />
		);

		return createModal(modal)({ open: true });
	}

	public static executeOk = (title: string, message: string) => {
		const modal = ({ open, close }) =>
		(
			<OkModal open={open} title={title} message={message} onClose={close} />
		);

		return createModal(modal)({ open: true });
	}

	public static execute = (title: string, message: string, buttons: Array<IModalButton>) => {
		const modal = ({ open, close }) =>
		(
			<Modal open={open} title={title} message={message} buttons={buttons} onClose={close} />
		);

		return createModal(modal)({ open: true });
	}
}

export { ModalExecutor }