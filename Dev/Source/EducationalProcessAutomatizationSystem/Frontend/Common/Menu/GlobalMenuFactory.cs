﻿using System;
using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.Common.GlobalConfiguration;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Constants;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.Menu
{
	public class GlobalMenuFactory
	{
		public IEnumerable<MenuItem> GetGlobalMenu()
		{
			return new List<MenuItem>
			{
				new MenuItem(
					"/Menu/Global/menu-main.svg",
					"Главная",
					Guid.NewGuid().ToString(),
					$"{SystemRoutingConfiguration.Protocol}://{SystemRoutingConfiguration.Domain}",
					Roles.Everybody),
				new MenuItem(
					"/Menu/Global/menu-prog.svg",
					"Система автоматизации документооборота",
					Guid.NewGuid().ToString(),
					$"{SystemRoutingConfiguration.Protocol}://{SystemRoutingConfiguration.ProgrammesManagementSystemDomain}",
					Roles.Everybody),
			};
		}
	}
}
