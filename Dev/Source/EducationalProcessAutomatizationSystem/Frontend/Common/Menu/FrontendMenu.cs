﻿using System.Collections.Generic;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.Menu
{
	public class FrontendMenu : IFrontendMenu
	{
		public FrontendMenu(IEnumerable<MenuItem> globalMenu, IEnumerable<MenuItem> menu)
		{
			GlobalMenu = new List<MenuItem>(globalMenu);

			Menu = new List<MenuItem>(menu);
		}

		public IReadOnlyCollection<MenuItem> GlobalMenu { get; }

		public IReadOnlyCollection<MenuItem> Menu { get; }
	}
}
