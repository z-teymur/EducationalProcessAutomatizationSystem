﻿using System.Collections.Generic;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.Menu
{
	public interface IFrontendMenu
	{
		IReadOnlyCollection<MenuItem> GlobalMenu { get; }

		IReadOnlyCollection<MenuItem> Menu { get; }
	}
}
