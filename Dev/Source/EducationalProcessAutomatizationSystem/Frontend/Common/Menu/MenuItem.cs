﻿using System.Collections.Generic;

namespace EducationalProcessAutomatizationSystem.Frontend.Common.Menu
{
	public class MenuItem
	{
		public MenuItem(string icon, string name, string key, string route, params string[] allowedRoles)
		{
			Icon = icon;
			Name = name;
			Key = key;
			Route = route;
			_allowedRoles = allowedRoles;
		}

		public string Icon { get; private set; }

		public string Name { get; private set; }

		public string Route { get; set; }

		public string Key { get; private set; }

		public bool Selected { get; private set; }

		public IEnumerable<string> GetRoles() => _allowedRoles;


		public void Select()
		{
			Selected = true;
		}

		private readonly IEnumerable<string> _allowedRoles;
	}
}
