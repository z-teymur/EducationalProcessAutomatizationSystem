$hosts = "$env:windir\System32\drivers\etc\hosts"
$content = Get-Content $hosts

Write-Host "Checking localhost binding"
if(-Not ($content -Like "*.development.internal*")){
    ""                                      | Add-Content -PassThru $hosts
    "127.0.0.1 development.internal"        | Add-Content -PassThru $hosts
    "127.0.0.1 prog.development.internal"   | Add-Content -PassThru $hosts
    "127.0.0.1 kibana.development.internal" | Add-Content -PassThru $hosts

    Write-Host "Added bindings to  *.development.internal"
}


Write-Host "Restoring npm packages"
$startPoint = Split-Path $PSScriptRoot -Parent
$packages = Get-ChildItem -Path $startPoint -Filter "package.json" -Recurse -ErrorAction SilentlyContinue -Force
ForEach($package in $packages){
    if($package.FullName -Like "*node_modules*") { continue }
    if($package.FullName -Like "*bin*Debug*") { continue }

    Write-Host "Restoring packages for $($package.FullName)"
    Start-Process -FilePath "npm" -ArgumentList "install" -WorkingDirectory $package.DirectoryName -NoNewWindow -Wait
    Start-Process -FilePath "npm" -ArgumentList "run build" -WorkingDirectory $package.DirectoryName -NoNewWindow -Wait
}