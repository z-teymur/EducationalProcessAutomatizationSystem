﻿SET IDENTITY_INSERT [dbo].[TrainingForm] ON

INSERT INTO [dbo].[TrainingForm]([Id], [Name])
VALUES	(1, N'Очная'),
		(2, N'Заочная')

SET IDENTITY_INSERT [dbo].[TrainingForm] OFF

----------------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Speciality] ON

INSERT INTO [dbo].[Speciality]([Id], [FacultyId], [TrainingFormId], [DepartmentId], [Code], [Name])
VALUES (1, 1, 1, 1, N'09.03.02', N'Информационные системы и технологии')

SET IDENTITY_INSERT [dbo].[Speciality] OFF

----------------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Profile] ON

INSERT INTO [dbo].[Profile]([Id], [SpecialityId] ,[Name])
VALUES	(1, 1, N'Информационные системы и технологии обработки цифрового контента'),
		(2, 1, N'Технологии смешанной реальности')

SET IDENTITY_INSERT [dbo].[Profile] OFF

----------------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Qualification] ON

INSERT INTO [dbo].[Qualification]([Id], [Name])
VALUES	(1, N'Абитурент'),
		(2, N'Бакалавр'),
		(3, N'Магистр')

SET IDENTITY_INSERT [dbo].[Qualification] OFF

----------------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Curriculum] ON

INSERT INTO [dbo].[Curriculum]([Id], [Year], [ProfileId], [QualificationId])
VALUES	(1, 2020, 1, 2)

SET IDENTITY_INSERT [dbo].[Curriculum] OFF

----------------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Discipline] ON

INSERT INTO [dbo].[Discipline]([Id], [Uid], [Name], [ProgrammeId], [AssessmentFundId])
VALUES 
	(1, NEWID(), N'Астрология', NULL, NULL),
	(2, NEWID(), N'Сопромат', NULL, NULL),
	(3, NEWID(), N'Зельеварение', NULL, NULL),
	(4, NEWID(), N'Базы данных', NULL, NULL)

SET IDENTITY_INSERT [dbo].[Discipline] OFF

----------------------------------------------------------------------------------------

INSERT INTO [dbo].[CurriculumDiscipline] ([CurriculumId],[DisciplineId])
VALUES	(1, 1), (1, 2), (1, 3), (1, 4)

----------------------------------------------------------------------------------------

INSERT INTO [dbo].[DisciplineEmployee] ([DisciplineId] ,[EmployeeId])
VALUES	(1, 1), (2, 1), (3, 1), (4, 1)

----------------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[TimeSchedule] ON

INSERT INTO [dbo].[TimeSchedule] ([Id], [DisciplineId], [Term], [Lectures], [Seminars], [Practices], [Laboratorys], [Individuals], [Controlls], [Interactives], [Exams], [Credits])
VALUES							 (1,	1,				2,		0,			0,			8,			 0,				4,				0,			0,				0,		  0),
								 (2,	1,				3,		0,			0,			0,			 0,				0,				5,			0,				2,		  0)

SET IDENTITY_INSERT [dbo].[TimeSchedule] OFF