﻿/* [MainService] */
BEGIN TRY
USE [master]
CREATE LOGIN [MainService] WITH PASSWORD = '12345';
END TRY BEGIN CATCH END CATCH

BEGIN TRY
USE [EPASDatabase]
CREATE USER [MainServiceUsr] FOR LOGIN [MainService];
EXEC sp_addrolemember 'MainService', 'MainServiceUsr';
END TRY BEGIN CATCH END CATCH


/* [ProgrammesManagementSystemService] */
BEGIN TRY
USE [master]
CREATE LOGIN [ProgrammesManagementSystemService] WITH PASSWORD = '12345';
END TRY BEGIN CATCH END CATCH

BEGIN TRY
USE [EPASDatabase]
CREATE USER [ProgrammesManagementSystemServiceUsr] FOR LOGIN [ProgrammesManagementSystemService];
EXEC sp_addrolemember 'ProgrammesManagementSystemService', 'ProgrammesManagementSystemServiceUsr';
END TRY BEGIN CATCH END CATCH