﻿SET IDENTITY_INSERT [dbo].[University] ON

INSERT INTO [dbo].[University]([Id], [RectorId], [Name])
VALUES(1, 2, N'Московский политех')

SET IDENTITY_INSERT [dbo].[University] OFF

----------------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Subdivision] ON

INSERT INTO [dbo].[Subdivision]([Id], [UniversityId], [DirectorId], [Name])
VALUES(1, 1, 3, N'Высшая школа печати и медиаиндустрии')

SET IDENTITY_INSERT [dbo].[Subdivision] OFF

----------------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Faculty] ON

INSERT INTO [dbo].[Faculty]([Id], [SubdivisionId], [DirectorId], [Name])
VALUES(1, 1, 4, N'ИПИТ')

SET IDENTITY_INSERT [dbo].[Faculty] OFF

----------------------------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Department] ON

INSERT INTO [dbo].[Department] ([Id], [DirectorId], [Name])
VALUES (1, 5, N'Кафедра информатики и информационных технологий')

SET IDENTITY_INSERT [dbo].[Department] OFF
