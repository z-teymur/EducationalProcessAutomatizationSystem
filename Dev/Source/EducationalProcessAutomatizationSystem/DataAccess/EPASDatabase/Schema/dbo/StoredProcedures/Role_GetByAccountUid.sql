﻿CREATE PROCEDURE [dbo].[Role_GetByAccountUid]
	@Uid UNIQUEIDENTIFIER
AS
	SELECT	r.[Id],
			r.[Name],
			r.[Importance]
	FROM	[dbo].[Role] r
		JOIN	[dbo].[AccountRole] ar 
			ON	ar.[RoleId] =  r.[Id]
		JOIN	[dbo].[Account] a
			ON ar.[AccountId] =  a.[Id]
	WHERE	a.[Uid] = @Uid
