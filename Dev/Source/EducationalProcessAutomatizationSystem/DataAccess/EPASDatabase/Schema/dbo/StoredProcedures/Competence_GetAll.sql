﻿CREATE PROCEDURE [dbo].[Competence_GetAll]
	@year SMALLINT
AS
	SELECT	[Code],
			[Description]
	FROM	[dbo].[Competence]
	WHERE	[Year] = @year
