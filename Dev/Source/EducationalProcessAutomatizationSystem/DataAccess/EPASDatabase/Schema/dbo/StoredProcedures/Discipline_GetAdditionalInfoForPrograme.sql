﻿CREATE PROCEDURE [dbo].[Discipline_GetAdditionalInfoForPrograme]
	@disciplineUid UNIQUEIDENTIFIER
AS
	SELECT	c.[Year],
			d.[Name]	AS [DisciplineName],
			s.[Code]	AS [SpecialityCode],
			s.[Name]	AS [SpecialityName],
			p.[Name]	AS [ProfileName],
			q.[Name]	AS [QualificationName],
			tf.[Name]	AS [TrainingFormName]
	FROM dbo.Discipline d
	JOIN dbo.[CurriculumDiscipline] cd	ON cd.[DisciplineId] = d.Id
	JOIN dbo.[Curriculum] c				ON cd.[CurriculumId] = c.Id
	JOIN dbo.[Profile] p				ON p.[Id] = c.[ProfileId]
	JOIN dbo.[Speciality] s				ON s.[Id] = p.[SpecialityId]
	JOIN dbo.[Qualification] q			ON q.[Id] = c.[QualificationId]
	JOIN dbo.[TrainingForm] tf			ON tf.[Id] = s.[TrainingFormId]
	WHERE d.[Uid] = @disciplineUid
