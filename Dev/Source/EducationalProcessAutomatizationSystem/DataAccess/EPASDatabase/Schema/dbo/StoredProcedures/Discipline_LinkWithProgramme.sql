﻿CREATE PROCEDURE [dbo].[Discipline_LinkWithProgramme]
	@disciplineUid		UNIQUEIDENTIFIER = NULL,
	@programmeUid		UNIQUEIDENTIFIER = NULL
AS
	UPDATE Discipline
	SET		[ProgrammeId] = (SELECT [Id] FROM Programme WHERE [Uid] = @programmeUid)
	WHERE	[Uid] = @disciplineUid 
	AND		[ProgrammeId] IS NULL

	SELECT @@ROWCOUNT