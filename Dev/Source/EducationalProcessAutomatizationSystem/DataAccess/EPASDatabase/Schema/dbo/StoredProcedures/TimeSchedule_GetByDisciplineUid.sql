﻿CREATE PROCEDURE [dbo].[TimeSchedule_GetByDisciplineUid]
	@disciplineUid UNIQUEIDENTIFIER
AS
	SELECT	DISTINCT
		 [Term]
		,[Lectures]
		,[Seminars]
		,[Practices]
		,[Laboratorys]
		,[Individuals]
		,[Controlls]
		,[Interactives]
		,[Exams]
		,[Credits]
	FROM [dbo].[TimeSchedule] ts
	JOIN [dbo].[Discipline] d ON d.Id = ts.DisciplineId 
	WHERE d.[Uid] = @disciplineUid