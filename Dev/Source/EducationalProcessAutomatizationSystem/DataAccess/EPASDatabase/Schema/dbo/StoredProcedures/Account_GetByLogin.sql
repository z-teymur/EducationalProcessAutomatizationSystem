﻿CREATE PROCEDURE [dbo].[Account_GetByLogin]
	@Login NVARCHAR(MAX)
AS
	SELECT	[Uid],
			[Login],
			[Password],
			[PasswordChangedTimestamp],
			[PasswordChangeToken],
			[Active],
			[CreationTimestamp],
			[ModificationTimestamp],
			[RowVersion]
	FROM	[dbo].[Account] a
	WHERE	a.[Login] = @Login
