﻿CREATE PROCEDURE [dbo].[Programme_Save]
	@uid		UNIQUEIDENTIFIER = NULL,
	@content	VARBINARY(MAX),
	@rowVersion ROWVERSION = NULL
AS
	IF(EXISTS(SELECT 1 FROM [dbo].[Programme] WHERE [Uid] = @uid))
		UPDATE [dbo].[Programme] SET [Content] = @content 
		OUTPUT inserted.Uid
		WHERE [RowVersion] = @rowVersion
	ELSE
		INSERT INTO [dbo].[Programme] ([Uid], [Content]) 
		OUTPUT inserted.Uid
		VALUES (NEWID(), @content)	
