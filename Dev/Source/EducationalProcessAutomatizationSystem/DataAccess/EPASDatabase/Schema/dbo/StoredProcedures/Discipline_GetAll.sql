﻿CREATE PROCEDURE [dbo].[Discipline_GetAll]
AS
	SELECT	DISTINCT
			[Name]
	FROM	[dbo].[Discipline]
