﻿CREATE PROCEDURE [dbo].[Discipline_GetByAccountUid]
	@accountUid UNIQUEIDENTIFIER
AS
	SELECT	s.[Code]	AS [SpecialityCode],
			s.[Name]	AS [SpecialityName],
			p.[Name]	AS [ProfileName],
			d.[Name]	AS [DisciplineName],
			d.[Uid]		AS [DisciplineUid],
			prog.[Uid]	AS [ProgrammeUid]
	FROM	[dbo].[Discipline] d
	LEFT JOIN	[dbo].[Programme] prog
		ON	d.ProgrammeId = prog.Id
	JOIN	[dbo].[CurriculumDiscipline] cd
		ON cd.[DisciplineId] = d.[Id]
	JOIN	[dbo].[Curriculum] c 
		ON c.[Id] = cd.[CurriculumId]
	JOIN	[dbo].[Profile] p
		ON p.[Id] = c.[ProfileId]
	JOIN	[dbo].[Speciality] s
		ON s.[Id] = p.[SpecialityId]
	JOIN	[dbo].[DisciplineEmployee] de
		ON	d.[Id] = de.[DisciplineId]
	JOIN	[dbo].[Employee] e
		ON de.[EmployeeId] = e.[Id]
	JOIN	[dbo].[Account] a
		ON a.[Id] = e.[AccountId]
	WHERE a.[Uid] =  @accountUid