﻿CREATE PROCEDURE [dbo].[Programme_GetByUid]
	@uid UNIQUEIDENTIFIER
AS

	SELECT	[Id],
			[Uid],
			[Content],
			[RowVersion]
	FROM	[dbo].[Programme]
	WHERE	[Uid] =  @uid