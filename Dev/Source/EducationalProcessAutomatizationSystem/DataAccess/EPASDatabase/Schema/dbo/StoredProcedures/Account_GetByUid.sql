﻿CREATE PROCEDURE [dbo].[Account_GetByUid]
	@Uid UNIQUEIDENTIFIER
AS
	SELECT	[Uid],
			[Login],
			[Password],
			[PasswordChangedTimestamp],
			[PasswordChangeToken],
			[Active],
			[CreationTimestamp],
			[ModificationTimestamp],
			[RowVersion]
	FROM	[dbo].[Account] a
	WHERE	a.[Uid] = @Uid
