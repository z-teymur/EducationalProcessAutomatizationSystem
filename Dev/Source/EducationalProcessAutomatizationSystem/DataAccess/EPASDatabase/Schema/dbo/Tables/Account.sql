﻿CREATE TABLE [dbo].[Account]
(
	[Id]						BIGINT				NOT NULL IDENTITY,
	[Uid]						UNIQUEIDENTIFIER	NOT NULL,
	[Login]						NVARCHAR(50)		NOT NULL,
	[Password]					VARBINARY(128)		NOT NULL,
	[PasswordChangedTimestamp]	DATETIME2(7)			NULL,
	[PasswordChangeToken]		VARBINARY(128)			NULL,
	[Active]					BIT					NOT NULL	DEFAULT 1,
	[CreationTimestamp]			DATETIME2(7)		NOT NULL	DEFAULT GETUTCDATE(),
	[ModificationTimestamp]		DATETIME2(7)		NOT NULL	DEFAULT GETUTCDATE(),
	[RowVersion]				ROWVERSION			NOT NULL,
	CONSTRAINT [PK_Account]			PRIMARY KEY		([Id]),
	CONSTRAINT [AK_Account_Login]	UNIQUE			([Login])
)
