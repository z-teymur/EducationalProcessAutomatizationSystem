﻿CREATE TABLE [dbo].[TimeSchedule](
	[Id]			BIGINT	NOT NULL IDENTITY,
	[DisciplineId]	BIGINT	NOT NULL,
	[Term]			INT		NOT NULL,
	[Lectures]		INT		NOT NULL,
	[Seminars]		INT		NOT NULL,
	[Practices]		INT		NOT NULL,
	[Laboratorys]	INT		NOT NULL,
	[Individuals]	INT		NOT NULL,
	[Controlls]		INT		NOT NULL,
	[Interactives]	INT		NOT NULL,
	[Exams]			INT		NOT NULL,
	[Credits]		INT		NOT NULL,
	CONSTRAINT [PK_TimeSchedule] PRIMARY KEY ([Id]), 
	CONSTRAINT [FK_TimeSchedule_Discipline] FOREIGN KEY ([DisciplineId]) REFERENCES [dbo].[Discipline]([Id])
)