﻿CREATE TABLE [dbo].[Discipline](
	[Id]				BIGINT				NOT NULL IDENTITY,
	[Uid]				UNIQUEIDENTIFIER	NOT NULL,
	[Name]				NVARCHAR(100)		NOT NULL,
	[ProgrammeId]		BIGINT				NULL,
	[AssessmentFundId]	BIGINT				NULL,
	CONSTRAINT [PK_Discipline]					PRIMARY KEY ([Id]),
	CONSTRAINT [FK_Discipline_Programme]		FOREIGN KEY([ProgrammeId]) REFERENCES [dbo].[Programme] ([Id]),
	CONSTRAINT [FK_Discipline_AssessmentFund]	FOREIGN KEY([AssessmentFundId]) REFERENCES [dbo].[AssessmentFund] ([Id])
)
GO
-- CREATE UNIQUE NONCLUSTERED INDEX  [AK_Discipline_ProgrammeId]		ON [dbo].[Discipline]([ProgrammeId]) WHERE [ProgrammeId] IS NOT NULL;
GO
-- CREATE UNIQUE NONCLUSTERED INDEX  [AK_Discipline_AssessmentFundId]	ON [dbo].[Discipline]([AssessmentFundId]) WHERE [AssessmentFundId] IS NOT NULL;
