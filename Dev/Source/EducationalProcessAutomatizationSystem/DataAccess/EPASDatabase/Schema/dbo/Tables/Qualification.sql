﻿CREATE TABLE [dbo].[Qualification]
(
	[Id]	INT				NOT NULL IDENTITY,
	[Name]	NVARCHAR(50)	NOT NULL,
	CONSTRAINT [PK_Qualification] PRIMARY KEY ([Id]),
)
