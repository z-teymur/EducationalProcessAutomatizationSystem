﻿CREATE TABLE [dbo].[Speciality](
	[Id]				BIGINT			NOT NULL IDENTITY,
	[FacultyId]			INT				NOT NULL,
	[DepartmentId]		INT				NOT NULL,
	[TrainingFormId]	INT				NOT NULL,
	[Code]				NVARCHAR(15)	NOT NULL,
	[Name]				NVARCHAR(200)	NOT NULL,
	CONSTRAINT [PK_Speciality]				PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_Speciality_Department]	FOREIGN KEY([DepartmentId])		REFERENCES [dbo].[Department] ([Id]),
	CONSTRAINT [FK_Speciality_TrainingForm]	FOREIGN KEY([TrainingFormId])	REFERENCES [dbo].[TrainingForm] ([Id]),
	CONSTRAINT [FK_Speciality_Faculty]		FOREIGN KEY([FacultyId])		REFERENCES [dbo].[Faculty] ([Id])
)