﻿CREATE TABLE [dbo].[Profile](
	[Id]			BIGINT			NOT NULL IDENTITY,
	[SpecialityId]	BIGINT			NOT NULL,
	[Name]			NVARCHAR(200)	NOT NULL,
	CONSTRAINT [PK_Profile] PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_Profile_Speciality] FOREIGN KEY([SpecialityId]) REFERENCES [dbo].[Speciality] ([Id])
)