﻿CREATE TABLE [dbo].[Faculty](
	[Id]			INT				NOT NULL IDENTITY,
	[SubdivisionId]	INT				NOT NULL,
	[DirectorId]	BIGINT			NOT NULL,
	[Name]			NVARCHAR(200)	NOT NULL,
	CONSTRAINT [PK_Faculty]				PRIMARY KEY ([Id]),
	CONSTRAINT [AK_Faculty_DirectorId]	UNIQUE ([DirectorId]),
	CONSTRAINT [FK_Faculty_Employee]	FOREIGN KEY([DirectorId])		REFERENCES [dbo].[Employee] ([Id]),
	CONSTRAINT [FK_Faculty_Subdivision]	FOREIGN KEY([SubdivisionId])	REFERENCES [dbo].[Subdivision] ([Id])
)