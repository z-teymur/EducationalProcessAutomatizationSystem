﻿CREATE TABLE [dbo].[Personality](
	[Id]			BIGINT			NOT NULL IDENTITY,
	[Surname]		NVARCHAR(50)	NOT NULL,
	[Name]			NVARCHAR(50)	NOT NULL,
	[Patronymic]	NVARCHAR(50)		NULL,
	CONSTRAINT [PK_Personality] PRIMARY KEY ([Id])
)