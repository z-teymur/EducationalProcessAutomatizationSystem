﻿CREATE TABLE [dbo].[Subdivision](
	[Id]			INT				NOT NULL IDENTITY,
	[UniversityId]	INT				NOT NULL,
	[DirectorId]	BIGINT			NOT NULL,
	[Name]			NVARCHAR(50)	NOT NULL,
	CONSTRAINT [PK_Subdivision]				PRIMARY KEY ([Id]),
	CONSTRAINT [AK_Subdivision_DirectorId]	UNIQUE ([DirectorId]),
	CONSTRAINT [FK_Subdivision_Employee]	FOREIGN KEY([DirectorId])	REFERENCES [dbo].[Employee] ([Id]),
	CONSTRAINT [FK_Subdivision_University]	FOREIGN KEY([UniversityId])	REFERENCES [dbo].[University] ([Id])
)