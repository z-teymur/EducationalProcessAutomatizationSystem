﻿CREATE TABLE [dbo].[CurriculumDiscipline](
	[Id]			BIGINT	NOT NULL IDENTITY,
	[CurriculumId]	BIGINT	NOT NULL,
	[DisciplineId]	BIGINT	NOT NULL,
	CONSTRAINT [PK_CurriculumDiscipline] PRIMARY KEY ([Id]),
	CONSTRAINT [AK_CurriculumDiscipline_DisciplineId] UNIQUE ([DisciplineId]),
	CONSTRAINT [FK_CurriculumDiscipline_Curriculum] FOREIGN KEY ([CurriculumId]) REFERENCES [dbo].[Curriculum]([Id]),
	CONSTRAINT [FK_CurriculumDiscipline_Discipline] FOREIGN KEY ([DisciplineId]) REFERENCES [dbo].[Discipline]([Id])
)