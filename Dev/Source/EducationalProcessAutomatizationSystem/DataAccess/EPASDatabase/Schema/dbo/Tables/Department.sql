﻿CREATE TABLE [dbo].[Department](
	[Id]			INT				NOT NULL IDENTITY,
	[DirectorId]	BIGINT			NOT NULL,
	[Name]			NVARCHAR(200)	NOT NULL,
	CONSTRAINT [PK_Department]				PRIMARY KEY ([Id]),
	CONSTRAINT [AK_Department_DirectorId]	UNIQUE ([DirectorId]),
	CONSTRAINT [FK_Department_Employee]		FOREIGN KEY([DirectorId]) REFERENCES [dbo].[Employee] ([Id])
)