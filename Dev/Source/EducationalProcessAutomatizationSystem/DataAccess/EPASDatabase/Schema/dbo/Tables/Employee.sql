﻿CREATE TABLE [dbo].[Employee](
	[Id]			BIGINT	NOT NULL IDENTITY,
	[AccountId]		BIGINT	NOT NULL,
	[PersonalityId]	BIGINT	NOT NULL,
	CONSTRAINT [PK_Employee]				PRIMARY KEY ([Id]),
	CONSTRAINT [AK_Employee_AccountId]		UNIQUE ([AccountId]),
	CONSTRAINT [AK_Employee_PersonalityId]	UNIQUE ([PersonalityId]),
	CONSTRAINT [FK_Employee_Account]		FOREIGN KEY([AccountId])		REFERENCES [dbo].[Account] ([Id]),
	CONSTRAINT [FK_Employee_Personality]	FOREIGN KEY([PersonalityId])	REFERENCES [dbo].[Personality] ([Id]),
)