﻿CREATE TABLE [dbo].[Curriculum](
	[Id]				BIGINT		NOT NULL IDENTITY,
	[Year]				SMALLINT	NOT NULL,
	[ProfileId]			BIGINT		NOT NULL,
	[QualificationId]	INT			NOT NULL,
	CONSTRAINT [PK_Curriculum]					PRIMARY KEY ([Id]),
	CONSTRAINT [FK_Curriculum_Profile]			FOREIGN KEY([ProfileId])		REFERENCES [dbo].[Profile] ([Id]),
	CONSTRAINT [FK_Curriculum_Qualification]	FOREIGN KEY([QualificationId])	REFERENCES [dbo].[Qualification] ([Id])
)