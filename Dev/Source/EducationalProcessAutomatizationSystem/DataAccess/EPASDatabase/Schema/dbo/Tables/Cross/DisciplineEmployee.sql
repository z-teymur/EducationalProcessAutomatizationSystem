﻿CREATE TABLE [dbo].[DisciplineEmployee](
	[Id]			BIGINT	NOT NULL IDENTITY,
	[DisciplineId]	BIGINT	NOT NULL,
	[EmployeeId]	BIGINT	NOT NULL,
	CONSTRAINT [PK_DisciplineEmployee]				PRIMARY KEY ([Id]),
	CONSTRAINT [FK_DisciplineEmployee_Discipline]	FOREIGN KEY([DisciplineId])	REFERENCES [dbo].[Discipline] ([Id]),
	CONSTRAINT [FK_DisciplineEmployee_Employee]		FOREIGN KEY([EmployeeId])	REFERENCES [dbo].[Employee] ([Id])
)