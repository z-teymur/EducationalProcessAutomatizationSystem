﻿CREATE TABLE [dbo].[AccountRole]
(
	[AccountId]		BIGINT	NOT NULL IDENTITY,
	[RoleId]		INT		NOT NULL,
	CONSTRAINT [PK_AccountRole] PRIMARY KEY ([AccountId], [RoleId]),
	CONSTRAINT [FK_AccountRole_ToAccount_Id] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Account]([Id]),
	CONSTRAINT [FK_AccountRole_ToRole_Id] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role]([Id]), 
)
