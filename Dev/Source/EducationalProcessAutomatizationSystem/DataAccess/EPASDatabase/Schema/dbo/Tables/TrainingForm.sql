﻿CREATE TABLE [dbo].[TrainingForm]
(
	[Id]	INT				NOT NULL IDENTITY,
	[Name]	NVARCHAR(50)	NOT NULL,
	CONSTRAINT [PK_TrainingForm] PRIMARY KEY ([Id]),
)
