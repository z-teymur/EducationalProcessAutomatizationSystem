﻿CREATE TABLE [dbo].[Competence] (
	[Code]			NVARCHAR(10)	NOT NULL,
	[Description]	NVARCHAR(500)	NOT NULL,
	[Year]			SMALLINT		NOT NULL,
	CONSTRAINT [PK_Competence] PRIMARY KEY ([Code])
)