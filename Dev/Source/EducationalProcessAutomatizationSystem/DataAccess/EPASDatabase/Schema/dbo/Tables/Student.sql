﻿CREATE TABLE [dbo].[Student](
	[Id]			BIGINT	NOT NULL IDENTITY,
	[AccountId]		BIGINT		NULL,
	[PersonalityId]	BIGINT	NOT NULL,
	CONSTRAINT [PK_Student]					PRIMARY KEY ([Id]),
	CONSTRAINT [AK_Student_AccountId]		UNIQUE ([AccountId]),
	CONSTRAINT [AK_Student_PersonalityId]	UNIQUE ([PersonalityId]),
	CONSTRAINT [FK_Student_Account]			FOREIGN KEY([AccountId])		REFERENCES [dbo].[Account] ([Id]),
	CONSTRAINT [FK_Student_Personality]		FOREIGN KEY([PersonalityId])	REFERENCES [dbo].[Personality] ([Id])
)