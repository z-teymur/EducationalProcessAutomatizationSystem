﻿CREATE TABLE [dbo].[University](
	[Id]		INT				NOT NULL IDENTITY,
	[RectorId]	BIGINT			NOT NULL,
	[Name]		NVARCHAR(50)	NOT NULL,
	CONSTRAINT [PK_University]			PRIMARY KEY ([Id]),
	CONSTRAINT [AK_University_RectorId]	UNIQUE ([RectorId]),
	CONSTRAINT [FK_University_Employee]	FOREIGN KEY([RectorId]) REFERENCES [dbo].[Employee] ([Id])
)