﻿CREATE TABLE [dbo].[Role]
(
	[Id]					INT				NOT NULL IDENTITY,
	[Name]					NVARCHAR(50)	NOT NULL,
	[Importance]			INT				NULL,
	CONSTRAINT [PK_Role]	PRIMARY KEY		([Id])
)
