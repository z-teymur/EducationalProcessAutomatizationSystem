﻿CREATE ROLE	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[Discipline_GetByAccountUid]
TO	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[Discipline_GetAll] 
TO	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[Account_GetByUid]
TO	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[Role_GetByAccountUid]
TO	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[Programme_Save] 
TO	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[Programme_GetByUid] 
TO	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[Competence_GetAll]
TO	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[Discipline_GetAdditionalInfoForPrograme]
TO	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[TimeSchedule_GetByDisciplineUid]
TO	[ProgrammesManagementSystemService]

GO

GRANT EXECUTE 
ON	[dbo].[Discipline_LinkWithProgramme]
TO	[ProgrammesManagementSystemService]

