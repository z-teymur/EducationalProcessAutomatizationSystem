﻿CREATE ROLE	[MainService]

GO

GRANT EXECUTE 
ON	[dbo].Account_GetByLogin
TO	[MainService]

GO

GRANT EXECUTE 
ON	[dbo].Account_GetByUid
TO	[MainService]

GO

GRANT EXECUTE 
ON	[dbo].Role_GetByAccountUid 
TO	[MainService]