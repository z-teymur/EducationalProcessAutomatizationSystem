﻿namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Constants
{
	public static class Roles
	{
		public const string Everybody = nameof(Everybody);

		public const string Administrator = nameof(Administrator);

		public const string Student = nameof(Student);

		public const string Teacher = nameof(Teacher);
	}
}
