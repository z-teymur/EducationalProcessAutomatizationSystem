﻿namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Models
{
	public class DisciplineAdditionalInfoItem
	{
		public int Year { get; set; }

		public string DisciplineName { get; set; } = default!;

		public string SpecialityCode { get; set; } = default!;

		public string SpecialityName { get; set; } = default!;

		public string ProfileName { get; set; } = default!;

		public string QualificationName { get; set; } = default!;

		public string TrainingFormName { get; set; } = default!;
	}
}
