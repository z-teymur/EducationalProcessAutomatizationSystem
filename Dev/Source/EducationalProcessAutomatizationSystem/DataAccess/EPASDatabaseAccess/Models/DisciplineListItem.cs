﻿using System;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Models
{
	public class DisciplineListItem
	{
		public string SpecialityCode { get; set; } = default!;

		public string SpecialityName { get; set; } = default!;

		public string ProfileName { get; set; } = default!;

		public string DisciplineName { get; set; } = default!;

		public Guid DisciplineUid { get; set; } = default!;

		public Guid? ProgrammeUid { get; set; }
	}
}
