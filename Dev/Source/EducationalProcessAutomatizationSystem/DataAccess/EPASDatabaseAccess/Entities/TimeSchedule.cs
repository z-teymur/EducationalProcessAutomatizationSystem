﻿namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities
{
	public class TimeSchedule
	{
		public int Term { get; set; }

		public int Lectures { get; set; }

		public int Seminars { get; set; }

		public int Practices { get; set; }

		public int Laboratorys { get; set; }

		public int Individuals { get; set; }

		public int Controlls { get; set; }

		public int Interactives { get; set; }

		public int Exams { get; set; }

		public int Credits { get; set; }
	}
}
