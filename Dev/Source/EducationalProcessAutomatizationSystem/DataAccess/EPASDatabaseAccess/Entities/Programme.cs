﻿using System;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities
{
	public class Programme
	{
		public long Id { get; set; }

		public Guid Uid { get; set; }

		public byte[] Content { get; set; } = default!;

		public byte[] RowVersion { get; set; } = default!;
	}
}
