﻿namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities
{
	public class Competence
	{
		public string Code { get; set; } = default!;

		public string Description { get; set; } = default!;
	}
}
