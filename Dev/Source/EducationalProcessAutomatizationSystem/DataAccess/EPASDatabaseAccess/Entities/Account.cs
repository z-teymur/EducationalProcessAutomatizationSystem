﻿using System;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities
{
	public class Account
	{
		public long Id { get; set; }

		public Guid Uid { get; set; }

		public string Login { get; set; } = default!;

		public byte[] Password { get; set; } = default!;

		public DateTime PasswordChangedTimestamp { get; set; } = default!;

		public byte[] PasswordChangeToken { get; set; } = default!;

		public bool Active { get; set; }

		public DateTime CreationTimestamp { get; set; }

		public DateTime ModificationTimestamp { get; set; }

		public byte[] RowVersion { get; set; } = default!;
	}
}
