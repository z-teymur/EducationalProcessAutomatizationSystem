﻿namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities
{
	public class Role
	{
		public int Id { get; set; }

		public string Name { get; set; } = default!;

		public int Importance { get; set; }
	}
}
