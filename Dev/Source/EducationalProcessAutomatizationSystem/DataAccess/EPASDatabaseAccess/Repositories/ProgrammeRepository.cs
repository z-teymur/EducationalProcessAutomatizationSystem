﻿using System;
using EducationalProcessAutomatizationSystem.Common.Utils.Sql;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Exceptions;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public class ProgrammeRepository : IProgrammeRepository
	{
		public ProgrammeRepository(ISqlExecutor sqlExecutor)
		{
			_sqlExecutor = sqlExecutor;
		}

		public Programme GetByUid(Guid uid)
		{
			return _sqlExecutor.GetSingle<Programme>("[dbo].[Programme_GetByUid]", new { uid });
		}

		public Guid Save(Programme programme)
		{
			var programmeUid = _sqlExecutor.GetSingle<Guid>(
				"[dbo].[Programme_Save]",
				new
				{
					programme.Uid,
					programme.Content,
					programme.RowVersion,
				});

			if (programmeUid == null)
			{
				throw new OptimisticUpdateException();
			}

			return programmeUid;
		}

		private readonly ISqlExecutor _sqlExecutor;
	}
}
