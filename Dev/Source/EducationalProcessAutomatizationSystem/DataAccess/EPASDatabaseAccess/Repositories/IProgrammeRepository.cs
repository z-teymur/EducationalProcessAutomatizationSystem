﻿using System;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public interface IProgrammeRepository
	{
		Programme GetByUid(Guid uid);

		Guid Save(Programme programme);
	}
}