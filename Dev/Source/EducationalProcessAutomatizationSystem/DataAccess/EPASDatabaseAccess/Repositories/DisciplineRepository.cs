﻿using System;
using System.Collections.Generic;
using System.Linq;
using EducationalProcessAutomatizationSystem.Common.Utils.Sql;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Exceptions;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Models;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public class DisciplineRepository : IDisciplineRepository
	{
		public DisciplineRepository(ISqlExecutor sqlExecutor)
		{
			_sqlExecutor = sqlExecutor;
		}

		public List<DisciplineListItem> GetByAccountUid(Guid accountUid)
		{
			return _sqlExecutor.GetMany<DisciplineListItem>(
				"[dbo].[Discipline_GetByAccountUid]",
				new
				{
					AccountUid = accountUid.ToString()
				})
				.ToList();
		}

		public List<string> GetAll()
		{
			return _sqlExecutor.GetMany<string>("[dbo].[Discipline_GetAll]").ToList();
		}

		public DisciplineAdditionalInfoItem GetAdditionalInfo(Guid disciplineUid)
		{
			return _sqlExecutor.GetSingle<DisciplineAdditionalInfoItem>(
				"[dbo].[Discipline_GetAdditionalInfoForPrograme]",
				new
				{
					disciplineUid
				});
		}

		public void LinkWithProgramme(Guid disciplineUid, Guid programmeUid)
		{
			var updated = _sqlExecutor.GetSingle<bool>(
				"[dbo].[Discipline_LinkWithProgramme]",
				new
				{
					programmeUid,
					disciplineUid
				});

			if (!updated)
			{
				throw new OptimisticUpdateException();
			}
		}

		private readonly ISqlExecutor _sqlExecutor;
	}
}
