﻿using System;
using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.Common.Utils.Sql;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public class RoleRepository : IRoleRepository
	{
		public RoleRepository(ISqlExecutor sqlExecutor)
		{
			_sqlExecutor = sqlExecutor;
		}

		public IEnumerable<Role> GetByAccountUid(Guid uid)
		{
			return _sqlExecutor.GetMany<Role>("[dbo].[Role_GetByAccountUid]", new { uid });
		}

		private readonly ISqlExecutor _sqlExecutor;
	}
}
