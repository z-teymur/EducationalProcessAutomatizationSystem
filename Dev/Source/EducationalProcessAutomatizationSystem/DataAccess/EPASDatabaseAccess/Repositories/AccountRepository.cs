﻿using System;
using EducationalProcessAutomatizationSystem.Common.Utils.Sql;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public class AccountRepository : IAccountRepository
	{
		public AccountRepository(ISqlExecutor sqlExecutor)
		{
			_sqlExecutor = sqlExecutor;
		}

		public Account GetByUid(Guid uid)
		{
			return _sqlExecutor.GetSingle<Account>("[dbo].[Account_GetByUid]", new { uid });
		}

		public Account GetByLogin(string login)
		{
			return _sqlExecutor.GetSingle<Account>("[dbo].[Account_GetByLogin]", new { login });
		}


		private readonly ISqlExecutor _sqlExecutor;
	}
}
