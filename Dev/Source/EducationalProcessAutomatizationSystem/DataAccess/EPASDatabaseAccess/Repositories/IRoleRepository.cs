﻿using System;
using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public interface IRoleRepository
	{
		IEnumerable<Role> GetByAccountUid(Guid uid);
	}
}