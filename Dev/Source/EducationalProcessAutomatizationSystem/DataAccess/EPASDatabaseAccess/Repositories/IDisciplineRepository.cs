﻿using System;
using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Models;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public interface IDisciplineRepository
	{
		List<DisciplineListItem> GetByAccountUid(Guid accountUid);

		List<string> GetAll();

		DisciplineAdditionalInfoItem GetAdditionalInfo(Guid disciplineUid);

		void LinkWithProgramme(Guid disciplineUid, Guid programmeUid);
	}
}