﻿using System;
using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public interface ITimeScheduleRepository
	{
		List<TimeSchedule> GetByDisciplineUid(Guid disciplineUid);
	}
}