﻿using System;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public interface IAccountRepository
	{
		Account GetByUid(Guid uid);

		Account GetByLogin(string login);
	}
}