﻿using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public interface ICompetenceRepository
	{
		IEnumerable<Competence> GetAll(int year);
	}
}