﻿using System.Collections.Generic;
using EducationalProcessAutomatizationSystem.Common.Utils.Sql;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public class CompetenceRepository : ICompetenceRepository
	{
		public CompetenceRepository(ISqlExecutor sqlExecutor)
		{
			_sqlExecutor = sqlExecutor;
		}

		public IEnumerable<Competence> GetAll(int year)
		{
			return _sqlExecutor.GetMany<Competence>("[dbo].[Competence_GetAll]", new { year });
		}

		private readonly ISqlExecutor _sqlExecutor;
	}
}
