﻿using System;
using System.Collections.Generic;
using System.Linq;
using EducationalProcessAutomatizationSystem.Common.Utils.Sql;
using EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Entities;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Repositories
{
	public class TimeScheduleRepository : ITimeScheduleRepository
	{
		public TimeScheduleRepository(ISqlExecutor sqlExecutor)
		{
			_sqlExecutor = sqlExecutor;
		}

		public List<TimeSchedule> GetByDisciplineUid(Guid disciplineUid)
		{
			return _sqlExecutor.GetMany<TimeSchedule>(
					"[dbo].[TimeSchedule_GetByDisciplineUid]",
					new
					{
						DisciplineUid = disciplineUid.ToString()
					})
				.ToList();
		}

		private readonly ISqlExecutor _sqlExecutor;
	}
}
