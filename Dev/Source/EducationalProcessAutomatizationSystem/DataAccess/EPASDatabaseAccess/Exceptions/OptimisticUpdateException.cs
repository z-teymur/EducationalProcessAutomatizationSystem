﻿using System;

namespace EducationalProcessAutomatizationSystem.DataAccess.EPASDatabaseAccess.Exceptions
{
	public class OptimisticUpdateException : Exception
	{
	}
}
